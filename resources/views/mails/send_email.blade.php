@component('mail::message')
# Infinity Delivers
# Hello 

{{$name}}
<br>
{{$email}}
<br>
{{$contact}}
<br>
{{$company}}
<br>
{{$contact_description}}
<br>
<p>{{$message}}</p>

Thanks,<br>
Infinity Deliveries
@endcomponent
