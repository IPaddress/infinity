@component('mail::message')
BOOK DEMO<br>
INFINITY DELIVERIES<br><br>

Name: {{$fullname}}<br>
Email: {{$email}}<br>
Contact #: {{$contact}}<br>
Company: {{$company}}<br>
Message: {{$compose_message}}<br><br>
Thanks,<br>
Infinity Deliveries
@endcomponent
