<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
     
        @yield('title')
        <link rel="shortcut icon" href="{{ asset('img/apple-touch-icon.png') }}">
        <link href="{{asset('/vendor/aos/aos.css')}}" rel="stylesheet">
        <link href="{{asset('/vendor/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
        <link href="{{asset('/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
        <link href="{{asset('/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
        <link href="{{asset('/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
        <link href="{{asset('/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
        <link href="{{asset('/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
        <link rel="stylesheet" href="{{asset('/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{asset('/css/owl.theme.default.min.css')}}">
        <!-- Template Main CSS File -->
        <link href="{{asset('/css/style.css')}}" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap" rel="stylesheet">
        <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
        <link rel="stylesheet" type="text/css" href="//reports.hrmdirect.com/employment/default/sm/settings/dynamic-embed/dynamic-iframe-embed-css.php" /> 
    </head>
    <body class="antialiased">
      @include('inc.navbar')
        @yield('content')
      @include('inc.footer')
    </body>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="{{asset('/vendor/aos/aos.js')}}"></script>
    <script src="{{asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('/vendor/glightbox/js/glightbox.min.js')}}"></script>
    <script src="{{asset('/vendor/php-email-form/validate.js')}}"></script>
    <script src="{{asset('/vendor/purecounter/purecounter.js')}}"></script>
    <script src="{{asset('/vendor/swiper/swiper-bundle.min.js')}}"></script>
    <script src="{{asset('/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('/js/wow.js')}}"></script>
    <script src="{{asset('/js/main.js')}}"></script>
    <script>
      new WOW().init();

    </script>
    @yield('script')
</html>
