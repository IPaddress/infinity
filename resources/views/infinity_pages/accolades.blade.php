@extends('app.apps')
  @section('title')
    <title>Accolades|infinity</title> 
  @endsection
@section('content')

<style>
#accolades-imgbadge {
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  background-color: white;
  border-radius: 50%;
  box-shadow: 0 0 15px grey;
  margin: 0 15px 100px 0;
}
</style>

<!-- ======= Hero Section ======= -->
  <section id="accolades" style="margin-bottom: 80px;">
    <div class="container">
      <div class="row  d-flex align-items-center">
        <div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1 hero-margin-desktop text-center">
          <div data-aos="zoom-out">
            <h1>Awards and Recognition</span></h1>
            <h2>We are regularly recognized as leaders in the industry. We also receive recognition <br> and awards for our overall performance as a company, our dynamic workplace <br>culture, our contributions to the community and our client advocacy. Here are some <br>of our most recent achievements.</h2>
            <div class="text-center">
              <a href="#accoladesContent" class="btn-get-started scrollto green-btn">View Awards</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
      <defs>
        <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
      </defs>
      <g class="wave1">
        <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
      </g>
      <g class="wave2">
        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
      </g>
      <g class="wave3">
        <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
      </g>
    </svg>
  </section><!-- End Hero -->

  <div id="accoladesContent">
      <div class="accoladesBadge">
        
        <div class="card" style="">
          <div class="card-body">
              <img src="{{asset('/img/Accolades/inc500-1.png')}}" style="max-width: 100%; border-radius: 50%" class="img-responsive" />
          </div>
        </div>

        <div class="card" style="box-shadow: -1px 1px 18px -2px rgba(0,0,0,0.5);border-radius:50%;margin: 10px">
          <div class="card-body">
            <img src="{{asset('/img/Accolades/aaisp-1.png')}}" style="max-width: 100%; border-radius: 50%" class="img-responsive" />
          </div>
        </div>

        <div class="card" style="box-shadow: -1px 1px 18px -2px rgba(0,0,0,0.5);border-radius:50%;margin: 10px">
          <div class="card-body">
            <img src="{{asset('/img/Accolades/aaisp-2.png')}}" style="max-width: 100%; border-radius: 50%" class="img-responsive" />
          </div>
        </div>

        <div class="card" style="box-shadow: -1px 1px 18px -2px rgba(0,0,0,0.5);border-radius:50%;margin: 10px">
          <div class="card-body">
            <img src="{{asset('/img/Accolades/stevies-1.png')}}" style="max-width: 100%; border-radius: 50%" class="img-responsive" />
          </div>
        </div>

        <div class="card" style="box-shadow: -1px 1px 18px -2px rgba(0,0,0,0.5);border-radius:50%;margin: 10px">
          <div class="card-body">
            <img src="{{asset('/img/Accolades/stevies-2.png')}}" style="max-width: 100%; border-radius: 50%" class="img-responsive" />
          </div>
        </div>

        <div class="card" style="box-shadow: -1px 1px 18px -2px rgba(0,0,0,0.5);border-radius:50%;margin: 10px">
          <div class="card-body">
            <img src="{{asset('/img/Accolades/stevies-2.png')}}" style="max-width: 100%; border-radius: 50%" class="img-responsive" />
          </div>
        </div>

        <div class="card" style="box-shadow: -1px 1px 18px -2px rgba(0,0,0,0.5);border-radius:50%;margin: 10px">
          <div class="card-body">
            <img src="{{asset('/img/Accolades/aaisp-3.png')}}" style="max-width: 100%; border-radius: 50%" class="img-responsive" />
          </div>
        </div>

        <div class="card" style="box-shadow: -1px 1px 18px -2px rgba(0,0,0,0.5);border-radius:50%;margin: 10px">
          <div class="card-body">
            <img src="{{asset('/img/Accolades/aaisp-4.png')}}" style="max-width: 100%; border-radius: 50%" class="img-responsive" />
          </div>
        </div>
        
        <div class="card" style="box-shadow: -1px 1px 18px -2px rgba(0,0,0,0.5);border-radius:50%;margin: 10px">
          <div class="card-body">
            <img src="{{asset('/img/Accolades/aaisp-5.png')}}" style="max-width: 100%; border-radius: 50%" class="img-responsive" />
          </div>
        </div>

        <div class="card" style="box-shadow: -1px 1px 18px -2px rgba(0,0,0,0.5);border-radius:50%;margin: 10px">
          <div class="card-body">
            <img src="{{asset('/img/Accolades/pace-1.png')}}" style="max-width: 100%; border-radius: 50%" class="img-responsive" />
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="d-flex justify-content-center wow animate__animated animate__fadeInUp animate__delay-0.5s">
            <img src="{{asset('/img/Accolades/aaisp.png')}}" style="max-width: 100%; margin-bottom: 50px" class="img-responsive" />
          </div>
          <div class="d-flex justify-content-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="padding:10px 100px;">
              <p style="text-align:center; margin-bottom: 50px;font-family: 'Roboto', sans-serif;font-weight:300;">According to their official website, "Each year the AA-ISP honors individuals, organizations, and <br>service providers at its Annual Leadership Summit for their outstanding contribution to inside <br>Sales. Award recipients are selected by the AA-ISP Awards Committee which is compromised of <br> AA-ISP Executives and Advisory Board Members"</p>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="d-flex justify-content-center wow animate__animated animate__fadeInUp animate__delay-0.5s">
          <img src="{{asset('/img/Accolades/stevies.png')}}" style="max-width: 100%; margin-bottom: 50px" class="img-responsive" />
        </div>
        <div class="d-flex justify-content-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="padding:10px 100px;">
          <p style="text-align:center; margin-bottom: 50px;font-family: 'Roboto', sans-serif;font-weight:300;">According to their official website, "The Stevie Award trophy is one of the world's most <br> coveted prizes. Since 2002 the Gold Stevie Award has been conferred for achievement in <br> business to organizations and individuals in more than 60 nations"</p>
        </div>
      </div>

      <div class="container">
        <div class="d-flex justify-content-center wow animate__animated animate__fadeInUp animate__delay-0.5s">
          <img src="{{asset('/img/Accolades/inc500.png')}}" style="max-width: 100%; margin-bottom: 50px" class="img-responsive" />
        </div>
        <div class="d-flex justify-content-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="padding:10px 100px;">
            <p style="text-align:center; margin-bottom: 50px;font-family: 'Roboto', sans-serif;font-weight:300;">According to their official website, "The Inc. 500 ranks privately held companies according to <br> averaged year-over-year sales growth over the past four years. With approximately 75% of all <br> new job creation in the U.S coming from small businesses, the Inc. 500 is a prescient indicator <br> of the companies and industries that are driving the economy forward"</p>
        </div>
      </div>

      <div class="container">
          <div class="d-flex justify-content-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="padding:10px 100px;">
            <img src="{{asset('/img/Accolades/pace.png')}}" style="max-width: 100%; margin-bottom: 50px" class="img-responsive" />
            <img src="{{asset('/img/Accolades/pace-1.png')}}" style="max-width: 100%; margin-bottom: 50px" class="img-responsive" />
        </div>
        <div class="d-flex justify-content-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="padding:10px 100px;">
            <p style="text-align:center; margin-bottom: 50px;font-family: 'Roboto', sans-serif;font-weight:300;">According to their official website, "The PACE-SR0 incorporates aspectsof governmental <br>regulations and consumer protection rules for contact centers. The goal of the PACE-SR0 is to <br> assure a positive telecommunication services experience for consumer and provide an <br> objective system that reinforces companies' commitment to government compliance"</p>
        </div>
      </div>

</div>


@endsection
@section('script')
@endsection
