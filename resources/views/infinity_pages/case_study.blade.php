@extends('app.apps')
  @section('title')
    <title>Case Study|infinity</title> 
  @endsection
@section('content')
     <!-- ======= Hero Section ======= -->
  <section id="case-study" style="margin-bottom: 80px;">
    <div class="container">
      <div class="row d-flex align-items-center">
        <div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1  hero-margin-desktop text-center">
          <div data-aos="zoom-out">
            <h1>Case Study</span></h1>
            <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor <br> incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</h2>
            <div class="text-center">
              <a href="#caseStudyList" class="btn-get-started scrollto green-btn">View List</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
      <defs>
        <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
      </defs>
      <g class="wave1">
        <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
      </g>
      <g class="wave2">
        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
      </g>
      <g class="wave3">
        <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
      </g>
    </svg>
  </section><!-- End Hero -->


  <div class="container case-study-content-container" id="caseStudyList">
    <h2 class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s">Lorem  <span>Ipsum</span> Dolor</h2>
    <p class="text-center px-md-5 mx-md-5 wow animate__animated animate__fadeInUp animate__delay-0.5s" style="margin-bottom: 80px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure .</p>    
    <div class="row justify-content-center mb-5">
        <div class="col-xl-3 wow animate__animated animate__zoomIn animate__delay-500ms">
          <img src="{{asset('/img/case-study/1.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
        </div>
        <div class="col-xl-6 wow animate__animated animate__fadeInRightBig">
            <p class="mb-0 mt-3" style="color:#9F9F9F"><small>Marketing & Revenue </small></p>
            <p class="mb-0" style="font-size: 25px">Market Share & Record Revenue</p>
            <p>A large US brand struggled with its #2 industry position, & developing a cost--effective way to reach target customers to compete with the #1 brand. Client had used a field sales team to sell its product and turned to large call-c...</p>
            <a href="{{route('case_study_details')}}"  id="btn-list-case" class="py-2 px-3">View Case</a>
          </div>
      </div>

      <div class="row justify-content-center mb-5">
        <div class="col-xl-3 wow animate__animated animate__zoomIn animate__delay-500ms">
          <img src="{{asset('/img/case-study/2.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
        </div>
        <div class="col-xl-6 wow animate__animated animate__fadeInRightBig">
            <p class="mb-0 mt-3" style="color:#9F9F9F"><small>Achievement </small></p>
            <p class="mb-0" style="font-size: 25px">Achieving A New 100%</p>
            <p >A global brand leader in digital advertising struggled with a go-to-market strategy for an acquisition program. After multiple attempts to launch the program with other partners, they turned to Infinity...</p>
            <a href="{{route('case_study_details')}}" id="btn-list-case"  class="py-2 px-3">View Case</a>
          </div>
      </div>

      <div class="row justify-content-center mb-5">
        <div class="col-xl-3 wow animate__animated animate__zoomIn animate__delay-500ms">
          <img src="{{asset('/img/case-study/3.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
        </div>
        <div class="col-xl-6 wow animate__animated animate__fadeInRightBig">
            <p class="mb-0 mt-3" style="color:#9F9F9F"><small>Solutions </small></p>
            <p  class="mb-0" style="font-size: 25px">B2B Software Solutions</p>
            <p>An established US provider of speech and imaging solutions was using a traditional telemarketing provider for its outbound sales.
              The client became increasingly frustrated with the provider’s lack...</p>
              <a href="{{route('case_study_details')}}"  id="btn-list-case"  class="py-2 px-3">View Case</a>
            </div>
      </div>
  </div>

@endsection

