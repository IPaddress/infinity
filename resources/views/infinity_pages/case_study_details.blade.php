@extends('app.apps')
@section('title')
  <title>Case Study|infinity</title> 
@endsection
@section('content')
     <!-- ======= Hero Section ======= -->
  <section id="case-study" style="margin-bottom: 80px;">
    <div class="container">
      <div class="row d-flex align-items-center">
        <div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1  hero-margin-desktop text-center">
          <div data-aos="zoom-out">
            <h1>Case Study Details</span></h1>
            <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore<br> magna aliqua. Ut enim ad minim veniam.</h2>
            <div class="text-center">
              <a href="#caseStudy" class="btn-get-started scrollto green-btn">Study</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
      <defs>
        <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
      </defs>
      <g class="wave1">
        <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
      </g>
      <g class="wave2">
        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
      </g>
      <g class="wave3">
        <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
      </g>
    </svg>
  </section><!-- End Hero -->


  <div class="container case-study-content-container" id="caseStudy" >
    <h2 class="text-center mb-5 wow animate__animated animate__fadeInUp animate__delay-0.5s">Market Share  <span> & Record Revenue</span></h2>
    <div class="row justify-content-center">
        <div class="col-xl-5 wow animate__animated animate__fadeInLeftBig">
            <p style="font-size:25px;font-weight:400;color:#0078C8">Background</p>
             <p>A large US brand struggled with its #2 industry position, & developing a cost--effective way to reach target customers to compete with the #1 brand. </p> 
            <p> Client had used a field sales team to sell its product and turned to large call-center company to build inside sales team.</p>
            <p> The call center delivered dismal results, and the client turned to Infinity to build and scale its inside sales strategy. </p>
          
        </div>
        <div class="col-xl-5 wow animate__animated animate__zoomIn animate__delay-500ms">
            <img src="{{asset('/img/case-study/case-view.png')}}" alt="Avatar" style="max-width:100%;border-radius:20px;" class="img-responsive">
        </div>
      </div>
  </div>

  <div class="container-fluid case-study-details wow animate__animated animate__zoomIn animate__delay-500ms">
        <div class="row" style="color:#fff;">
            <div class="col-xl-6 px-5 py-5" style="background-color: #0078C8">
                <h2 style="font-size:25px;color:#fff;">Results</h2>
                <ol>
                  <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                  <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                  <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                  <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                  <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                </ol> 
            </div>
            <div class="col-xl-6  px-5 py-5" style="background-color:#82BE00;">
                <h2 style="font-size:25px;color:#fff;">Cost Saving</h2>
                <ul>
                  <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                  <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                  <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                  <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                  <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                </ul>
            </div>
        </div>
  </div>


@endsection

