@extends('app.apps')
@section('title')
      <title>Home|infinity</title> 
@endsection
@section('content')
     <!-- ======= Hero Section ======= -->
  <section id="hero" style="margin-bottom: 80px;">
    <div class="container">
      <div class="row d-flex align-items-center">
        <div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1  hero-margin-desktop text-center">
          <div class="mt-5" data-aos="zoom-out" >
            <h1>Get Customers. Keep Customers.</span></h1>
            <h2 class="px-5">We support our clients through the entire customer life cycle. From lead generation and <br>acquisition to customer service and retention.</h2>
            <div class="text-center">
              <a href="#firstPage" class="btn-get-started scrollto green-btn">Get Started</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
      <defs>
        <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
      </defs>
      <g class="wave1">
        <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
      </g>
      <g class="wave2">
        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
      </g>
      <g class="wave3">
        <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
      </g>
    </svg>
  </section><!-- End Hero -->

<div class="what-is-infinity-container" id="firstPage">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <p style="font-family: 'Poppins', sans-serif;font-weight:700">What <span >Infinity</span> Offers</p>
      </div>
    </div>
  </div>
</div>

  <div class="container" id="infinity-offers-container">
    <div class="row pt-4">
        <div class="col-xl-4 mb-3 px-0 wow animate__animated animate__zoomIn animate__delay-0.8s">
          <div class="d-flex justify-content-center align-items-center">
          <div class="flip-card" style="cursor: context-menu">
            <div class="flip-card-inner">
              <div class="flip-card-front" style="border: 1px solid #0078c8;">
                <img src="{{asset('/img/What Infinity Offers/strategy.png')}}" alt="Avatar" class="img-responsive">
              </div>
              <div class="flip-card-back">
                  <p id="strategy" class="mb-2">Strategy</p>
                    <p class="text-center mb-0">Go-To-Market Plan</p>
                    <p class="text-center mb-0">Revenue Plan & ROI</p>
                    <p class="text-center mb-0">Addressable Market</p>
                    <p class="text-center mb-0">Organizational Alignment</p>
                  </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-4 mb-3 px-0 wow animate__animated animate__zoomIn animate__delay-0.8s">
        <div class="d-flex justify-content-center align-items-center">
        <div class="flip-card" style="cursor: context-menu">
          <div class="flip-card-inner">
            <div class="flip-card-front" style="border: 1px solid #90d200;">
              <img src="{{asset('/img/What Infinity Offers/system.png')}}" alt="Avatar" class="img-responsive">
            </div>
            <div class="flip-card-back-green">
              <p id="system" class="mb-2">Systems</p>
              <p class="text-center mb-0">Sales Performance Analytics</p>
              <p class="text-center mb-0">Campaign Analytics</p>
              <p class="text-center mb-0">Customer Analytics</p>
              <p class="text-center mb-0">Support Systems</p>
              <p class="text-center mb-0">Compliance</p>
            </div>
          </div>
        </div>
      </div>
    </div>

      <div class="col-xl-4 mb-3  px-0 wow animate__animated animate__zoomIn animate__delay-0.8s">
        <div class="d-flex justify-content-center align-items-center">
        <div class="flip-card" style="cursor: context-menu">
          <div class="flip-card-inner">
            <div class="flip-card-front" style="border: 1px solid #0078c8;">
              <img src="{{asset('/img/What Infinity Offers/execution.png')}}" alt="Avatar" class="img-responsive">
            </div>
            <div class="flip-card-back">
              <p id="execution" class="mb-2">Execution</p>
              <p class="text-center mb-0">Team Structure & Culture</p>
              <p class="text-center mb-0">Sales Process</p>
              <p class="text-center mb-0">Performance Management</p>
              <p class="text-center mb-0">Human Talent Training </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>

  
  <div class="container" id="award-winning-revenue-system">
    <h2 class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-family: 'Poppins', sans-serif;font-weight:700">Award-Winning <span>Revenue System:</span> Buyerlytics®</h2>
      <div class="row justify-content-center">
        <div class="col-xxl-10 wow animate__animated animate__fadeInUp animate__delay-0.5s">
          <p class="text-center"><strong>Buyerlytics®</strong> is our differentiator! Infinity’s proprietary Buyerlytics® revenue
            system ensures programs are successfully launched, piloted, and scaled. </p>     
          <p class="text-center"> Companies of all sizes struggle with low sales productivity,
            customer churn and market share. With Buyerlytics®, you’ll have the necessary
            components to develop an actionable go-to-market plan, staffed with a right fit
            team. Our company promise includes a high-performance sales culture that
            enables teams to maximize data, technology, and our people to achieve and
            exceed our client’s goals.</p> 
          <p class="text-center">Want to know more?</p>
          <div class="d-flex justify-content-center">
            <a href="#" data-bs-toggle="modal" data-bs-target="#infinity" style="color: #82be00;"><i class="fas fa-play-circle fa-3x" ></i></a>
          </div>
        </div>
      </div>
  </div>



<!-- --------------------------------
Modal Video Infinity
----------------------------------->
  <div class="modal fade" id="infinity" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row justify-content-end">
                <div
                    style="width: 30px; height: 30px; border-radius: 50%; text-align: right; background: #1489EA; margin: 25px 25px 0 25px; display: flex; justify-content: center; align-items: center">
                    <i class="fal fa-times fa-3x"
                        style="cursor:pointer; text-align: right; font-size: 25px; color: white"
                        data-bs-dismiss="modal"></i>
                </div>
              </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-12 pb-5">
                          <video id="infinity-video" src="" loop autoplay style="max-width: 100%;"></video>
                          {{-- <img src="{{asset('/img/hero.gif')}}" id="solution-img" class="img-responsive" style="max-width:100%;"/> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


  <div class="container" id="solutions-container">
    <div class="row justify-content-center mb-5  ">
      <div class="col-xl-6 inbound-container wow animate__animated animate__fadeInRightBig ">
        <h1 style="font-family: 'Poppins', sans-serif;font-weight:700">Inbound <span>Sales</span></h1>
        <p class="mb-5 pe-lg-5 pe-sm-0">Dedicated Account Executives, with the right industry experience and demeanor
          to be successful, are the backbone of your inbound sales campaigns. Designed
          as an extension of your business, we help build the necessary and seamless
          touch points for your customers.</p>
        <a href="{{route('inbound_sales')}}" class="py-2 px-3">Get Customers</a>
      </div>
      <div class="col-xl-4 wow animate__animated animate__fadeInLeftBig ">
        <div class="d-flex justify-content-center">
          <img src="{{asset('/img/inbound 1.png')}}" id="solution-img" class="img-responsive" />
        </div>    
      </div>
    </div>

    <div class="row justify-content-center mb-5 outbound-container" >
      <div class="col-xl-4 wow animate__animated animate__fadeInRightBig">
        <div class="d-flex justify-content-center">
          <img src="{{asset('/img/outbound 3.png')}}" id="solution-img" class="img-responsive" />
        </div>        
      </div>
      <div class="col-xl-6 pe-5 mt-5 text-sm-start text-lg-end mb-3 wow animate__animated animate__fadeInLeftBig ">
        <p id="solution-titles" style="font-family: 'Poppins', sans-serif;font-weight:700;color:#000">Outbound <span style="color:#000">Sales</span></p>
        <p class="mb-5 ps-lg-5 ps-sm-0 ">Infinity’s outbound sales programs drive results by using outcome-oriented KPIs and dedicated cross-functional teams. These programs are built with Account Executives that have a “hunter” sales mentality and a strong competitive nature. They are supported by cross-functional management focused on outcomes, not just effort.</p>
        <a href="{{route('outbound_sales')}}" style="background-color: #0078c8; color: #fff;border-radius: 50px;" class="py-2 px-3">Get Customers</a>
      </div>
    </div>


    <div class="row justify-content-center mb-5 mb-3 customer-service">
      <div class="col-xl-6  mt-5 wow  animate__animated animate__fadeInRightBig ">
        <p id="solution-titles" style="font-family: 'Poppins', sans-serif;font-weight:700;color:#000">Customer <span style="color:#000">Service</span></p>
        <p class="mb-5 pe-lg-5 pe-sm-0">Infinity’s customer service campaigns include loyalty programs, customer support, reminder series, surveys, and call overflow and backstopping. When your resources just aren’t enough, we’re here to fill in with the necessary support to help you keep up with volume and avoid inactive customers.</p>
        <a href="{{route('customer_service')}}" id="solution-get-cx" class="py-2 px-3">Keep Customers</a>
      </div>
      <div class="col-xl-4 my-3 wow  animate__animated animate__fadeInLeftBig ">
        <div class="d-flex justify-content-center">
          <img src="{{asset('/img/customer 2.png')}}" id="solution-img" class="img-responsive mb-3" />
        </div>    
      </div>
    </div>
</div>



<div class="container mb-5 industry-titles">
    <h2 class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-family: 'Poppins', sans-serif;font-weight:700">Industries  <span style="color:#82BE00">We Serve</span></h2>    
    <p class="text-center px-3 mb-5 wow animate__animated animate__fadeInUp animate__delay-0.5s">Infinity has the right solution for your industry</p> 
            
  <div class="row justify-content-center">
     <div class="col-xl-2 mb-3 px-2  wow animate__animated animate__zoomIn animate__delay-500ms" id="img-zoom-container"> 
        <div class="custom-img-zoom-container" style="cursor: context-menu">
          <img id="img-gallery"  src='{{asset('/img/Industries We Serve/automotive.webp')}}' alt=''>
              <div class="img-zoom-overlay">                
                  <div class="text">
                    <p style="font-family: 'Roboto', sans-serif;font-weight:300;margin-top:15px;font-size:14px;">Automotive</p>
                  </div> 
              </div>
        </div>
     </div>

     <div class="col-xl-2 px-2 mb-3  wow animate__animated animate__zoomIn animate__delay-500ms" id="img-zoom-container"> 
      <div class="custom-img-zoom-container" style="cursor: context-menu">
        <img id="img-gallery" src='{{asset('/img/Industries We Serve/digital-marketing.webp')}}' alt='' >
            <div class="img-zoom-overlay">                
                <div class="text">
                  <p style="font-family: 'Roboto', sans-serif;font-weight:300;margin-top:15px;font-size:14px;">Digital Marketing</p>
                </div> 
            </div>
      </div>
   </div>

   
   <div class="col-xl-2 px-2 mb-3  wow animate__animated animate__zoomIn animate__delay-500ms" id="img-zoom-container"> 
      <div class="custom-img-zoom-container" style="cursor: context-menu">
        <img id="img-gallery" src='{{asset('/img/Industries We Serve/telecommunications.webp')}}' alt=''>
            <div class="img-zoom-overlay">                
                <div class="text">
                  <p style="font-family: 'Roboto', sans-serif;font-weight:300;margin-top:15px;font-size:14px;">Telecommunications</p>
                </div> 
            </div>
      </div>
    </div>

    <div class="col-xl-2 px-2 mb-3   wow animate__animated animate__zoomIn animate__delay-500ms" id="img-zoom-container"> 
      <div class="custom-img-zoom-container" style="cursor: context-menu">
        <img id="img-gallery" src='{{asset('/img/Industries We Serve/healthcare.webp')}}' alt='' >
            <div class="img-zoom-overlay">                
                <div class="text">
                  <p style="font-family: 'Roboto', sans-serif;font-weight:300;margin-top:15px;font-size:14px;">Healthcare</p>
                </div> 
            </div>
      </div>
    </div>

    <div class="col-xl-2 px-2 mb-3  wow animate__animated animate__zoomIn animate__delay-500ms" id="img-zoom-container"> 
      <div class="custom-img-zoom-container" style="cursor: context-menu">
        <img id="img-gallery"  src='{{asset('/img/Industries We Serve/logistic.webp')}}' alt='' >
            <div class="img-zoom-overlay">                
                <div class="text">
                  <p style="font-family: 'Roboto', sans-serif;font-weight:300;margin-top:15px;font-size:14px;">Logistic</p>
                </div> 
            </div>
      </div>
    </div>

    <div class="col-xl-2 px-2 mb-3  wow animate__animated animate__zoomIn animate__delay-500ms" id="img-zoom-container"> 
      <div class="custom-img-zoom-container" style="cursor: context-menu">
        <img id="img-gallery" src='{{asset('/img/Industries We Serve/software_development.webp')}}' alt='' >
            <div class="img-zoom-overlay">                
                <div class="text">
                  <p style="font-family: 'Roboto', sans-serif;font-weight:300;margin-top:15px;font-size:14px;">Software & Technology</p>
                </div> 
            </div>
      </div>
    </div>
  </div>
</div>


{{-- <div class="what-offer-section">
  <div class="container" style="margin-bottom: 80px;">
        <p id="case-study-titles" class="text-center  mb-0 wow animate__animated animate__fadeInUp animate__delay-0.5s">Case Studies  <span style="color:#82BE00">& Stories</span></p>
        <p class="text-center mb-4 wow animate__animated animate__fadeInUp animate__delay-0.5s"  style="font-family: 'Roboto', sans-serif;font-weight:300;">We work together and make a customer satisfied.</p>     
         
          <div id="carouselExampleIndicators" class="carousel slide wow animate__animated animate__fadeInUp animate__delay-0.5s" data-bs-ride="carousel">
            <div class="carousel-inner mt-3">
              <div class="carousel-item active">
                <div class="container">
                  <div class="row justify-content-center">
                    <div class="col-xl-4">
                      <img src="{{asset('/img/graph.png')}}" id="case-study-graph" class="img-responsive" />
                    </div>
                    <div class="col-xl-6 ">
                      <p id="case-study-carousel-intro" class="mb-0 mt-3">Marketing & Revenue</p>
                      <p id="case-study-carousel-title" >Market Share & Record Revenue</p>
                      <p id="case-study-description">A large US brand struggled with its #2 industry position, & developing a cost--effective way to reach target customers to compete with the #1 brand. Client had used a field sales team to sell its product and turned to large call-c...</p>
                      <a href="{{route('case_study')}}" class="btn btn-success green-btn ms-0" >See All Studies</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="container">
                  <div class="row justify-content-center">
                    <div class="col-xl-4">
                      <img src="{{asset('/img/case-study/2.png')}}" id="case-study-graph" class="img-responsive" />
                    </div>
                    <div class="col-xl-6">
                      <p id="case-study-carousel-intro" class="mb-0 mt-3">Achievement</p>
                      <p id="case-study-carousel-title" >Achieving A New 100%</p>
                      <p id="case-study-description">A global brand leader in digital advertising struggled with a go-to-market strategy for an acquisition program. After multiple attempts to launch the program with other partners, they turned to Infinity...</p>
                      <a href="{{route('case_study')}}" class="btn btn-success green-btn ms-0" >See All Studies</a>
                    </div>
                  </div>
                </div>
              </div>
             <div class="carousel-item">
                <div class="container">
                  <div class="row justify-content-center">
                    <div class="col-xl-4 ">
                      <img src="{{asset('/img/case-study/3.png')}}" id="case-study-graph" class="img-responsive" />
                    </div>
                    <div class="col-xl-6 ">
                      <p id="case-study-carousel-intro" class="mb-0 mt-3">Solutions</p>
                      <p id="case-study-carousel-title" >B2B Software Solutions</p>
                      <p id="case-study-description">An established US provider of speech and imaging solutions was using a traditional telemarketing provider for its outbound sales. The client became increasingly frustrated with the provider’s lack...</p>
                      <a href="{{route('case_study')}}" class="btn btn-success green-btn ms-0" >See All Studies</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
              <i class="fal fa-chevron-left fa-2x " style="color:#0078C8"></i>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
              <i class="fal fa-chevron-right fa-2x" style="color:#0078C8"></i>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
      </div>
    </div> --}}




<div class="container py-4 mb-5 wow animate__animated animate__fadeInUp animate__delay-0.5s">
  <p id="home-testimonial-title" class="text-center mb-3 " style="font-family: 'Poppins', sans-serif;font-weight:700">What Our Partners  <span style="color:#000">Say About Us</span></p>
	<div class="row">
		<div class="col-md-12">
			<div class="lc-block">
				<div id="testimonial-slider" class="carousel slide" data-bs-ride="carousel">
              <div class="carousel-indicators">
                <a id="indicator-for-partners" style="background-color:#0078C8;width:15px;height:15px;border-radius:50%;" href="#" data-bs-target="#testimonial-slider" data-bs-slide-to="0"  class=""> <i class="fas fa-circle"></i></a>
                <a id="indicator-for-partners" style="background-color:#0078C8;width:15px;height:15px;border-radius:50%;" href="#" data-bs-target="#testimonial-slider" data-bs-slide-to="1"  class="active"> <i class="fas fa-circle"></i></a>
                <a id="indicator-for-partners" style="background-color:#0078C8;width:15px;height:15px;border-radius:50%;" href="#" data-bs-target="#testimonial-slider" data-bs-slide-to="2"  class=""><i class="fas fa-circle"></i></a>
              </div>
         

					<div class="carousel-inner" style="font-weight:300;">
						<div class="carousel-item active">
                <div class="row justify-content-center mb-5">
                    <div class="col-md-2 col-6">
                      <img alt="" class="rounded-circle" src="img/testimonials1.jpg" style="width:100%;" loading="lazy">
                    </div>
                </div>
                <div class="row justify-content-center mb-5">
                  <div class="col-md-8 col-12"  style="font-family: 'Roboto', sans-serif;font-weight:300;">
                    <p class="text-center">“We call Infinity our Innovation Center for being our go-to vendor for designing and proving campaigns. They set the Gold-Standard in high value sales.”</p>
                    <p id="home-slider-position" class="text-center mb-0" >- VP Sales Leader</p>
                    <p id="home-slider-company" class="text-center mb-0" >World-Wide Digital Marketing Brand</p>
                  </div>
                </div>
					  	</div>
					  <div class="carousel-item ">
              <div class="row justify-content-center mb-3">
                <div class="col-md-2 col-6">
                  <img alt="" class="rounded-circle" src="https://images.unsplash.com/photo-1569779213435-ba3167dde7cc?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" style="width:100%;" loading="lazy">
                </div>
            </div>
            <div class="row justify-content-center mb-5">
              <div class="col-md-8 col-12">
                <p class="text-center">“Infinity has proven year after year to be an invaluable partner for us in driving our sales revenue ever higher, with their true sales professionals that are real product experts, and with management expertise at the top to help us drive the results.”</p>
                <p class="text-center mb-0" style="color:#0078C8;font-weight:900;font-size:20px;">- VP of Account Management</p>
                <p class="text-center mb-0"  >Global Leader in Auditory and Imaging Software</p>
              </div>
            </div>
						</div>
						<div class="carousel-item">
              <div class="row justify-content-center mb-3">
                <div class="col-md-2 col-6">
                  <img alt="" class="rounded-circle" src="img/testimonials2.jpg" style="width:100%;" loading="lazy">
                </div>
            </div>
            <div class="row justify-content-center mb-5">
              <div class="col-md-8 col-12">
                <p class="text-center">“Today’s QBR was the best I have ever participated in, whether in my current or previous roles. I’m excited about the opportunities that lay ahead of us and the focus that Infinity’s team has demonstrated...”</p>
                <p class="text-center mb-0" style="color:#0078C8;font-weight:900;font-size:20px;">- VP of Channel Management</p>
                <p class="text-center mb-0" style="color:#0078C8;font-weight:300;font-size:20px;" > Global Information Solutions Company</p>
              </div>
            </div>
					</div>



					<!-- Left and right controls -->
					<a class="carousel-control-prev" href="#testimonial-slider" data-bs-slide="prev">
            <i class="fal fa-chevron-left fa-2x " style="color:#0078C8"></i>
					</a>
					<a class="carousel-control-next" href="#testimonial-slider" data-bs-slide="next">
            <i class="fal fa-chevron-right fa-2x" style="color:#0078C8"></i>
					</a>
				</div>


			</div>
		</div><!-- /col -->
	</div>
</div>
</div>

@endsection
@section('script')
    <script>
      $("#infinity").on("shown.bs.modal", function(e) {
        $("#infinity-video").attr(
          "src","{{asset('img/Infinity Delivers - Buyerlytics - Whiteboard Animation_720P HD.mp4')}}"
        );
      });

      $("#infinity").on("hide.bs.modal", function(e) {
        $("#infinity-video").attr("src", "");
      });


    </script>
@endsection


