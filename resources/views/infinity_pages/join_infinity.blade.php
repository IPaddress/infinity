@extends('app.apps')
  @section('title')
    <title>Join Our Team|infinity</title> 
  @endsection
@section('content')
     <!-- ======= Hero Section ======= -->
  <section id="join-infinity" style="margin-bottom: 80px;">
    <div class="container ">
      <div class="row d-flex align-items-center">
        <div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1  hero-margin-desktop text-center">
          <div data-aos="zoom-out">
            <h1>Join Infinity</h1>
            <h2>Infinity attracts and retains the best people in the industry, because we believe in <br>providing an inclusive, supportive, and inspiring work environment for our teams. We <br> believe that good is never good enough. That’s why we’re so committed to building <br> the best teams and helping each employee achieve their full potential. </h2>
            <div class="text-center">
              <a href="#" class="btn-get-started scrollto green-btn" data-bs-toggle="modal" data-bs-target="#joblist">Explore Opportunities</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
      <defs>
        <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
      </defs>
      <g class="wave1">
        <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
      </g>
      <g class="wave2">
        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
      </g>
      <g class="wave3">
        <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
      </g>
    </svg>
  </section><!-- End Hero -->




  <div class="container join-infinity-container px-5">
    <h2 class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-family: 'Poppins', sans-serif;font-weight:700;">It's Not a Job!  <span>It's a Career!</span></h2> 
    <p class="text-center px-md-5 mx-md-5 wow animate__animated animate__fadeInUp animate__delay-0.5s" style="margin-bottom: 80px;">With a customer engagement career at Infinity, you’ll learn life-long skills including sales, customer service, <br> empathy and active listening. We offer world-class training solutions, experience with some of the latest <br> technology applications, but most importantly, you’ll have the opportunity to grow and advance.</p>    
    <div class="row justify-content-center">
      <div class="col-xl-5 ps-0 pe-0">
        <img src="{{asset('/img/Join Infinity/career.webp')}}" alt="Avatar" style="max-width:100%;border-bottom-left-radius: 20px;border-top-left-radius: 20px;" class="img-responsive " >
      </div>
      <div class="col-xl-5 card-infinity-story p-0 ps-5">
          <p class="mb-3 mt-5">At Infinity, we empower you to solve problems and help people, while working with some of the most admired brands in the world. We offer competitive wages, benefits, and career growth opportunities. Most importantly, your voice and opinion are respected – in fact, we encourage it! </p>      
      </div>
    </div>
  </div>

  <div class="container  join-infinity-container px-5" style="margin-bottom: 80px;">
    <p class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-family: 'Poppins', sans-serif;font-weight:700;color:#000;font-size:40px;">Perks at Infinity</p>
    <p class="text-center px-md-5 mx-md-5 wow animate__animated animate__fadeInUp animate__delay-0.5s" style="margin-bottom: 80px;">We take care of our own here (hint: that could be you). Our benefits and rewards mean we cover <br> some of your biggest needs with some of the coolest offerings. We already think we’re a pretty great <br> place to work. We’re just trying to rack up some bonus point.</p>    
   
    <div class="row justify-content-center">
          <div class="col-md-5 mb-3">
            <div class="card shadow" style="border-radius:10px;">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-2">
                    <img src="{{asset('/img/Join Infinity/party_every_pay_day.webp')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
                  </div>
                  <div class="col-md-10">
                    <h1 style="font-size: 19px;color:#0078C8;font-weight:600" class="mt-3">Party Every Pay Day</h1>
                  </div>
                  <div class="col-lg-12">
                    <p>Let’s start with the big one: Your work gets rewarded with <strong> competetive compensation and benefits. </strong> It really does pay to be on our team</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-5 mb-3">
            <div class="card shadow" style="border-radius:10px;">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-2">
                    <img src="{{asset('/img/Join Infinity/party_every_pay_day.webp')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
                  </div>
                  <div class="col-md-10">
                    <h1 style="font-size: 19px;color:#0078C8;font-weight:600"  class="mt-3">Get Some “You” Time</h1>
                  </div>
                  <div class="col-lg-12">
                    <p>Vacation? Staycation? Heck, let’s take a road trip. On top of paid holidays, chill out with <strong> paid time off (PTO) </strong>  that you can spend any way you want.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    
      <div class="row justify-content-center">
        <div class="col-md-5 mb-3">
          <div class="card shadow" style="border-radius:10px;">
            <div class="card-body">
              <div class="row">
                <div class="col-md-2">
                  <img src="{{asset('/img/Join Infinity/save_on_swag.webp')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
                </div>
                <div class="col-md-10">
                  <h1 style="font-size: 19px;color:#0078C8;font-weight:600"  class="mt-3">Save On Swag</h1>
                </div>
                <div class="col-lg-12">
                  <p>Wanna make your friends really jealous? You’ll get discounted access to the latest and greatest perks, products and services - plus other awesome items like tickets to events.</p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-5 mb-3">
          <div class="card shadow" style="border-radius:10px;">
            <div class="card-body">
              <div class="row">
                <div class="col-md-2">
                  <img src="{{asset('/img/Join Infinity/upgrade_your_skill_set.webp')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
                </div>
                <div class="col-md-10">
                  <h1 style="font-size: 19px;color:#0078C8;font-weight:600"  class="mt-3">Upgrade Your Skill Set</h1>
                </div>
                <div class="col-lg-12">
                  <p>You strike us as an over-achiever (don’t worry, its a compliment). Our <strong>training and development</strong>  programs are your ticket to expert status in your job.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    
</div>



     <div class="container infinity-container px-5" style="margin-bottom: 0px">
        <div class="row row-infinity justify-content-center">
            <div class="col-xl-5 col-xl-4 wow  animate__animated animate__fadeInRightBig">
              <img src="{{asset('/img/Join Infinity/about_employee.webp')}}" id="img-life-infinity" alt="Avatar" style="max-width:100%;" class="img-responsive">
            </div>
            <div class="col-xl-5 mb-5 col-xl-4 wow  animate__animated animate__fadeInLeftBig">
              <p class="mb-0 pt-0" style="color:#000;font-size:25px;font-family: 'Poppins', sans-serif;font-weight:bold">A Culture of Building Purposeful <br>Careers</p>
              <p class="mb-0">  Getting a paycheck is important. So is moving up in your career. What else matters to Infinity employees? Giving it back, paying it forward and making lives better.....one 
                interaction at a time, through our world-class culture, that 
                enables mission-focused work.</p>
              <ul style="font-family: 'Roboto', sans-serif;font-weight:300;">
                <li>Diversity, Equity & Inclusion Initiatives</li>
                <li>Charitable Giving</li>
                <li>Professional Development</li>
              </ul>
            </div>
        </div>

    

        <div class="row row-infinity justify-content-center">
            <div class="col-lg-5 mb-5 wow  animate__animated animate__fadeInRightBig">
              <p class="mb-0"style="color:#000;font-size:25px;font-family: 'Poppins', sans-serif;font-weight:bold;">A Great Culture Starts with People</p>
              <p class="mb-0"> At Infinity, there’s nothing we value more than our people. And it’s our people who create Infinity’s culture. From employee recognitions, fun contests to company-wide celebrations, we are committed to connecting with, and empowering, our teams so that they can bring their best every day.</p>
            </div>
            <div class="col-lg-5 mb-5  wow  animate__animated animate__fadeInLeftBig">
              <img src="{{asset('/img/Join Infinity/our_facilities.png')}}" id="img-life-infinity" alt="Avatar" style="max-width:100%;" class="img-responsive">
            </div>
        </div>
    </div>


    <div class="container join-infinity-container" style="margin-bottom: 80px;">
      <div class="row d-flex justify-content-center">
        <div class="col-xl-10 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
          <div class="overlay-container-infinity">
            <img src="{{asset('/img/Join Infinity/joblist.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
               <div class="joblisting-overlay">       
               </div>
               <div class="joblisting-text">
                <h1 class="mb-0" style="font-family: 'Poppins', sans-serif;font-weight:700">Ready to be part of Infinity </h1>
                <a href="#" data-bs-toggle="modal" data-bs-target="#joblist" style="color:#fff;font-weight:300;font-family: 'Poppins', sans-serif;">See All Job Openings </a>
              </div>            
            </div>   
         </div>  
      </div>
    </div>





  <div class="container infinity-container">
    <h2 class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-family: 'Poppins', sans-serif;font-weight:700">Diversity,  <span> Equity and</span> <span style="color: #0078C8"> Inclusion</span></h2>
    <p class="text-center px-md-5 mx-md-5 wow animate__animated animate__fadeInUp animate__delay-0.5s" style="margin-bottom: 80px;">Our differences make us stronger and bring us together as one team.</p>    

    
      <div class="d-flex flex-row justify-content-center diversity bd-highlight mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
        <div class="custom-disable-overlay-core-values wow animate__animated animate__zoomIn animate__delay-500ms">
          <div class="p-2 bd-highlight">  <img src="{{asset('/img/Join Infinity/disable.png')}}" alt="Avatar" style="max-width:100%;border:15px solid rgba(0, 148, 247, 0.75);border-radius:50%;" class="img-responsive"></div>
              <div class="disable-overlay-core-values">                
                  <div class="text-disable-core-values">
                    <p class="px-3">Disability</p>
                  </div> 
              </div>
        </div>


        <div class="custom-lgbt-overlay-core-values wow animate__animated animate__zoomIn animate__delay-500ms">
          <div class="p-2 bd-highlight">  <img src="{{asset('/img/Join Infinity/lgbtq.png')}}" alt="Avatar" style="max-width:100%;border:15px solid rgba(0, 148, 247, 0.75);;border-radius:50%;" class="img-responsive"></div>
              <div class="lgbt-overlay-core-values">                
                  <div class="text-lgbt-core-values">
                    <p class="px-3">LGBTQ</p>
                  </div> 
              </div>
        </div> 
      </div>

      <div class="d-flex flex-row justify-content-center bd-highlight mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
        <div class="custom-blackmen-overlay-core-values">
          <div class="p-2 bd-highlight">  <img src="{{asset('/img/Join Infinity/people with color.png')}}" alt="Avatar" style="max-width:100%;border:15px solid #82BE00;border-radius:50%;" class="img-responsive"></div>
            <div class="blackmen-overlay-core-values">                
              <div class="text-blackmen-core-values">
                <p class="px-3">People of Color</p>
              </div> 
            </div>
        </div>

        <div class="custom-girl-overlay-core-values wow animate__animated animate__zoomIn animate__delay-500ms">
          <div class="p-2 bd-highlight">  <img src="{{asset('/img/Join Infinity/women.png')}}" alt="Avatar" style="max-width:100%;border:15px solid rgba(0, 148, 247, 0.75);border-radius:50%;" class="img-responsive"></div>
              <div class="girl-overlay-core-values">                
                  <div class="text-girl-core-values">
                    <p class="px-3">Women</p>
                  </div> 
              </div>
        </div>
        
        <div class="custom-veteran-overlay-core-values wow animate__animated animate__zoomIn animate__delay-500ms">
          <div class="p-2 bd-highlight">  <img src="{{asset('/img/Join Infinity/veterans.png')}}" alt="Avatar" style="max-width:100%;border:15px solid #82BE00;border-radius:50%;" class="img-responsive"></div>
              <div class="veteran-overlay-core-values">                
                  <div class="text-veteran-core-values">
                    <p class="px-3">Veterans</p>
                  </div> 
              </div>
        </div>
      </div>
  </div>

  <!-- Diversity Modal -->
  
<div class="modal fade" id="asian" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="border-radius: 20px;">
      <div class="row justify-content-end">
        <div style="width: 30px; height: 30px; border-radius: 50%; text-align: right; background: #1489EA; margin: 25px 25px 0 25px; display: flex; justify-content: center; align-items: center">
          <i class="fal fa-times fa-3x" style="cursor:pointer; text-align: right; font-size: 25px; color: white" data-bs-dismiss="modal" ></i>
        </div>
      </div>
      <div class="modal-body">
        <p style="font-size:30px;font-weight:700;color:#82BE00; text-align: left; margin: 0 0 20px 50px"><span style="color:#0078C8 !important">Asian</span></p>
        <div class="d-flex justify-content-center" style="padding: 0 50px 10px 50px; min-height: 300px">
          <div style="min-height: 300px; min-width: 300px; margin-right: 40px">
            <img s  rc="{{asset('/img/Join Infinity/asian.png')}}" alt="Avatar" style="max-width:100%; border-radius: 20px" class="img-responsive mb-3">
          </div>
          <div>
            <details>
              <summary>
                This is Dropdown Box
              </summary>
               <p><kbd>Windows</kbd></p>
              <sample>Windows</sample>
               <p>sample 3</p>
            </details>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Rem architecto quidem dignissimos facilis doloremque velit tempore! Ut soluta accusamus tempora atque recusandae sequi impedit, repellat, obcaecati optio earum porro ipsum?</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="disable" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="border-radius: 20px;">
      <div class="row justify-content-end">
        <div style="width: 30px; height: 30px; border-radius: 50%; text-align: right; background: #1489EA; margin: 25px 25px 0 25px; display: flex; justify-content: center; align-items: center">
          <i class="fal fa-times fa-3x" style="cursor:pointer; text-align: right; font-size: 25px; color: white" data-bs-dismiss="modal" ></i>
        </div>
      </div>
      <div class="modal-body">
        <p style="font-size:30px;font-weight:700;color:#82BE00; text-align: left; margin: 0 0 20px 50px"><span style="color:#0078C8 !important">Disable</span></p>
        <div class="d-flex justify-content-center" style="padding: 0 50px 10px 50px; min-height: 300px">
          <div style="min-height: 300px; min-width: 300px; margin-right: 40px">
            <img src="{{asset('/img/Join Infinity/disable.png')}}" alt="Avatar" style="max-width:100%; border-radius: 20px" class="img-responsive mb-3">
          </div>
          <div>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Rem architecto quidem dignissimos facilis doloremque velit tempore! Ut soluta accusamus tempora atque recusandae sequi impedit, repellat, obcaecati optio earum porro ipsum?</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="hispanic" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="border-radius: 20px;">
      <div class="row justify-content-end">
        <div style="width: 30px; height: 30px; border-radius: 50%; text-align: right; background: #1489EA; margin: 25px 25px 0 25px; display: flex; justify-content: center; align-items: center">
          <i class="fal fa-times fa-3x" style="cursor:pointer; text-align: right; font-size: 25px; color: white" data-bs-dismiss="modal" ></i>
        </div>
      </div>
      <div class="modal-body">
        <p style="font-size:30px;font-weight:700;color:#82BE00; text-align: left; margin: 0 0 20px 50px"><span style="color:#82BE00 !important">Hispanic</span></p>
        <div class="d-flex justify-content-center" style="padding: 0 50px 10px 50px; min-height: 300px">
          <div style="min-height: 300px; min-width: 300px; margin-right: 40px">
            <img src="{{asset('/img/Join Infinity/hispanic.png')}}" alt="Avatar" style="max-width:100%; border-radius: 20px" class="img-responsive mb-3">
          </div>
          <div>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Rem architecto quidem dignissimos facilis doloremque velit tempore! Ut soluta accusamus tempora atque recusandae sequi impedit, repellat, obcaecati optio earum porro ipsum?</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="lgbtq" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="border-radius: 20px;">
      <div class="row justify-content-end">
        <div style="width: 30px; height: 30px; border-radius: 50%; text-align: right; background: #1489EA; margin: 25px 25px 0 25px; display: flex; justify-content: center; align-items: center">
          <i class="fal fa-times fa-3x" style="cursor:pointer; text-align: right; font-size: 25px; color: white" data-bs-dismiss="modal" ></i>
        </div>
      </div>
      <div class="modal-body">
        <p style="font-size:30px;font-weight:700;color:#82BE00; text-align: left; margin: 0 0 20px 50px"><span style="color:#0078C8 !important">LGBTQ</span></p>
        <div class="d-flex justify-content-center" style="padding: 0 50px 10px 50px; min-height: 300px">
          <div style="min-height: 300px; min-width: 300px; margin-right: 40px">
            <img src="{{asset('/img/Join Infinity/lgbtq.png')}}" alt="Avatar" style="max-width:100%; border-radius: 20px" class="img-responsive mb-3">
          </div>
          <div>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Rem architecto quidem dignissimos facilis doloremque velit tempore! Ut soluta accusamus tempora atque recusandae sequi impedit, repellat, obcaecati optio earum porro ipsum?</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="black" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="border-radius: 20px;">
      <div class="row justify-content-end">
        <div style="width: 30px; height: 30px; border-radius: 50%; text-align: right; background: #1489EA; margin: 25px 25px 0 25px; display: flex; justify-content: center; align-items: center">
          <i class="fal fa-times fa-3x" style="cursor:pointer; text-align: right; font-size: 25px; color: white" data-bs-dismiss="modal" ></i>
        </div>
      </div>
      <div class="modal-body">
        <p style="font-size:30px;font-weight:700;color:#82BE00; text-align: left; margin: 0 0 20px 50px"><span style="color:#82BE00 !important">Black</span></p>
        <div class="d-flex justify-content-center" style="padding: 0 50px 10px 50px; min-height: 300px">
          <div style="min-height: 300px; min-width: 300px; margin-right: 40px">
            <img src="{{asset('/img/Join Infinity/black.png')}}" alt="Avatar" style="max-width:100%; border-radius: 20px" class="img-responsive mb-3">
          </div>
          <div>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Rem architecto quidem dignissimos facilis doloremque velit tempore! Ut soluta accusamus tempora atque recusandae sequi impedit, repellat, obcaecati optio earum porro ipsum?</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="women" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="border-radius: 20px;">
      <div class="row justify-content-end">
        <div style="width: 30px; height: 30px; border-radius: 50%; text-align: right; background: #1489EA; margin: 25px 25px 0 25px; display: flex; justify-content: center; align-items: center">
          <i class="fal fa-times fa-3x" style="cursor:pointer; text-align: right; font-size: 25px; color: white" data-bs-dismiss="modal" ></i>
        </div>
      </div>
      <div class="modal-body">
        <p style="font-size:30px;font-weight:700;color:#82BE00; text-align: left; margin: 0 0 20px 50px"><span style="color:#0078C8 !important">Women</span></p>
        <div class="d-flex justify-content-center" style="padding: 0 50px 10px 50px; min-height: 300px">
          <div style="min-height: 300px; min-width: 300px; margin-right: 40px">
            <img src="{{asset('/img/Join Infinity/women.png')}}" alt="Avatar" style="max-width:100%; border-radius: 20px" class="img-responsive mb-3">
          </div>
          <div>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Rem architecto quidem dignissimos facilis doloremque velit tempore! Ut soluta accusamus tempora atque recusandae sequi impedit, repellat, obcaecati optio earum porro ipsum?</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="veteran" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="border-radius: 20px;">
      <div class="row justify-content-end">
        <div style="width: 30px; height: 30px; border-radius: 50%; text-align: right; background: #1489EA; margin: 25px 25px 0 25px; display: flex; justify-content: center; align-items: center">
          <i class="fal fa-times fa-3x" style="cursor:pointer; text-align: right; font-size: 25px; color: white" data-bs-dismiss="modal" ></i>
        </div>
      </div>
      <div class="modal-body">
        <p style="font-size:30px;font-weight:700;color:#82BE00; text-align: left; margin: 0 0 20px 50px"><span style="color:#82BE00 !important">Veteran</span></p>
        <div class="d-flex justify-content-center" style="padding: 0 50px 10px 50px; min-height: 300px">
          <div style="min-height: 300px; min-width: 300px; margin-right: 40px">
            <img src="{{asset('/img/Join Infinity/veteran.png')}}" alt="Avatar" style="max-width:100%; border-radius: 20px" class="img-responsive mb-3">
          </div>
          <div>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Rem architecto quidem dignissimos facilis doloremque velit tempore! Ut soluta accusamus tempora atque recusandae sequi impedit, repellat, obcaecati optio earum porro ipsum?</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>





  <div class="container infinity-count-data px-5">
    <h3 class="text-center mb-5 wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-family: 'Poppins', sans-serif;font-weight:700">Infinity’s Diverse & Highly-Skilled Workforce </h3>
      <div class="row justify-content-center">
        <div class="col-xl-4 mb-5 wow animate__animated animate__zoomIn animate__delay-500ms">
          <div class="card text-center py-4 shadow" style="border-radius:20px;">
            <div class="card-body">
              <div class="counter-section">
                <span class="c-section4" style="color:#0078C8;font-size:40px;font-weight:700">10+ years </span>
              </div>
                <p style="color:#82BE00;font-size:20px;">Average Leadership Tenure</p>
            </div>
          </div>
        </div>
        <div class="col-xl-4 mb-5 wow animate__animated animate__zoomIn animate__delay-500ms">
          <div class="card text-center py-4 shadow" style="border-radius:20px;">
            <div class="card-body ">
              <div class="counter-section">
                <span class="c-section4" style="color:#0078C8;font-size:40px;font-weight:700">58%</span>
              </div>
              <p style="color:#82BE00;font-size:20px;">Women</p>
            </div>
          </div>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-xl-4 mb-5 wow animate__animated animate__zoomIn animate__delay-500ms">
        <div class="card text-center py-4 shadow" style="border-radius:20px;">
          <div class="card-body">
            <div class="counter-section">
              <span class="c-section4" style="color:#0078C8;font-size:40px;font-weight:700">51%</span>
            </div>
              <p style="color:#82BE00;font-size:20px;">People of Color</p>
          </div>
        </div>
      </div>
      <div class="col-xl-4 mb-5 wow animate__animated animate__zoomIn animate__delay-500ms">
        <div class="card text-center py-4 shadow" style="border-radius:20px;">
          <div class="card-body ">
            <div class="counter-section">
              <span class="c-section4" style="color:#0078C8;font-size:40px;font-weight:700">85% </span>
            </div>
            <p style="color:#82BE00;font-size:20px;">Internal Promotion</p>
          </div>
        </div>
    </div>
  </div>
  </div>



  <div class="container infinity-container px-5">
    <div class="row">
        <div class="col-xl-12 wow animate__animated animate__zoomIn animate__delay-500ms">
            <div class="card card-apply-infinity-meeting">
              <div class="card-body  p-0">
                  <div class="row">
                    <div class="col-xl-6 ps-2">
                      <h3 class="pb-0 pt-0">Infinity’s Values</h3>    
                      <p class="pb-0">We consistently live Infinity’s values, including a responsibility with a bias for action; consistently exploring ways to be better; being passionate about client brands; celebrating our unique differences; and acting with respect, transparency, and authenticity. We are always searching for creative solutions to optimize our resources; it is through this growth, and the creation of genuine human interactions and connections, that we make a greater impact for our company, and our clients</p>
                    </div>
                    <div class="col-xl-6">   
                      <img src="{{asset('/img/Join Infinity/meeting.png')}}" alt="Avatar" style="max-width:100%; border-bottom-right-radius:20px;border-top-right-radius: 20px;" class="img-responsive">    
                    </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
  </div>


<div class="modal fade" id="joblist" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
      <div class="modal-content" style="border-radius: 10px;">
          <div class="row justify-content-end">
              <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                  <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
              </div>
          </div>
          <div class="modal-body">
              <div class="container">
                  <div class="row">
                      <div class="col-xl-12">
                        <div id="iframeHeightDiv" name="HRM Direct Career Site iFrame Container" align="center" >
                          <iframe id="inlineframe" name="HRM Direct Career Site iFrame" sandbox="allow-top-navigation allow-scripts allow-forms allow-popups allow-same-origin allow-popups-to-escape-sandbox" src="https://infinitydelivers.hrmdirect.com/employment/job-openings.php?search=true&" frameborder="0" allowtransparency="true" title="Career Site"> </iframe>
                       </div>
                      </div>
                   
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

@endsection
@section('script')
<script type="text/javascript" src="//reports.hrmdirect.com/employment/default/sm/settings/dynamic-embed/dynamic-iframe-embed-js.php"></script> 
@endsection
