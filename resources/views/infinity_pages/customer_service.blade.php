@extends('app.apps')
  @section('title')
      <title>Customer Service|infinity</title> 
  @endsection
@section('content')
     <!-- ======= Hero Section ======= -->
  <section id="customer_service" style="margin-bottom: 80px;">
    <div class="container">
      <div class="row d-flex align-items-center">
        <div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1  hero-margin-desktop text-center">
          <div data-aos="zoom-out">
            <h1>Customer Service</span></h1>
            <h2>Infinity’s B2B and B2C customer service campaigns, include loyalty programs, customer  <br>support, reminder series, surveys, and call overflow and backstopping. When your <br> resources just aren’t enough, we’re here to fill in with the necessary support to help you <br> keep up with volume and avoid inactive customers.</h2>
            <div class="text-center">
              <a href="#gotofirstpage" class="btn-get-started scrollto green-btn">Get Started</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
      <defs>
        <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
      </defs>
      <g class="wave1">
        <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
      </g>
      <g class="wave2">
        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
      </g>
      <g class="wave3">
        <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
      </g>
    </svg>
  </section><!-- End Hero -->


  <div class="container mb-3" id="gotofirstpage">
    <p class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-size:40px;font-weight:400;color:#000;font-family: 'Poppins', sans-serif;font-weight:700">Maximize  <span style="color:#000">Customer</span> <span style="color:#000">Relationships</span> </p>
      <div class="row">
          <div class="col-xl-12 mb-5">
            <p class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-family: 'Roboto', sans-serif;font-weight:300;">Infinity understands that every interaction with a customer should build value, not destroy it. We help enhance <br> your customer’s experience, using data-driven insights to refine processes and improve outcomes, with the <br> empathy only the human touch can bring.
            </p>     
        
          </div>
        </div>
    </div>

<div class="container" style="margin-bottom: 80px;">
  <div class="row justify-content-center">
    <div class="col-xl-5 wow animate__animated animate__fadeInRightBig">
      <img src="{{asset('/img/customer-service.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive"> 
    </div>
    <div class="col-xl-5 wow  animate__animated animate__fadeInLeftBig">
        <p style="font-family: 'Roboto', sans-serif;font-weight:300;">Infinity’s customer service programs include:</p>
        <ul style="font-family: 'Roboto', sans-serif;font-weight:300;">
          <li>Loyalty Programs</li>
          <li>Customer Support</li>
          <li>Reminder Series</li>
          <li>Survey</li>
          <li>Call Overflow</li>
          <li>Backstopping</li>
        </ul>
        <p style="font-family: 'Roboto', sans-serif;font-weight:300;">It’s our job to help clients maximize customer relationships. Our Account Executives react and respond to every communication, transforming key learnings into opportunities to optimize and evolve customer relationships.</p>
    </div>
  </div>
</div>


<div class="container"  style="margin-bottom:80px;">
  <p class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-size:40px;font-weight:400;color:#0078C8;font-family: 'Poppins', sans-serif;font-weight:700">Related <span style="color:#82BE00">Service</span></p>
  <p class="text-center mb-5 wow animate__animated animate__fadeInUp animate__delay-0.8s" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
  font-weight: 300;">In the age of the digital consumer, we help you pique interest, elevate experience, nurture leads, make sales, reduce churn and grow customer <br>brand affinity with our CX driven engagement services and solutions – that support throughout the entire customer lifecycle.</p>            
  <div class="row justify-content-center">
 
     <div class="col-xl-3 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
       <div class="custom_overlay">
        <img src="{{asset('img/inbound 1.png')}}" class="img-fluid" style="max-width: 100%; border-radius:10px;">   
          <a href="{{route('inbound_sales')}}">
            <div class="overlay">                
              <div class="text">
                <p id="solutions-title">Inbound Sales</p>
                <a href="{{route('inbound_sales')}}" style="background-color:#82BE00;color:#fff;border-radius: 50px;" class="py-2 px-3">Learn More</a>
              </div> 
            </div>
          </a>
       </div>   
      </div>
      <div class="col-xl-3 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
        <div class="custom_overlay">
          <img src="{{asset('img/outbound 3.png')}}" class="img-fluid" style="max-width: 100%;  border-radius:10px;">   
              <a href="{{route('outbound_sales')}}">
                <div class="overlay">                
                  <div class="text">
                    <p id="solutions-title">Outbound Sales</p>
                    <a href="{{route('outbound_sales')}}" style="background-color:#82BE00;color:#fff;border-radius: 50px;" class="py-2 px-3">Learn More</a>
                  </div> 
                </div>
              </a>
          </div>
      </div>
    </div>
  </div>



@endsection

