@extends('app.apps')
@section('title')
  <title>Contact Us|infinity</title> 
@endsection
@section('content')
     <!-- ======= Hero Section ======= -->
  <section id="contact-us" style="margin-bottom: 80px;">
    <div class="container">
      <div class="row d-flex align-items-center">
        <div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1  hero-margin-desktop text-center">
          <div data-aos="zoom-out">
            <h1>Contact Us</span></h1>
            <h2>Interested in learning more about how Infinity can help your organization? <br>Want to start exploring employment with us? Use the contact methods below, <br>to start the conversation.</h2>
            <div class="text-center">
              <a href="#gotofirstpage" class="btn-get-started scrollto green-btn">Talk To Us</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
      <defs>
        <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
      </defs>
      <g class="wave1">
        <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
      </g>
      <g class="wave2">
        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
      </g>
      <g class="wave3">
        <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
      </g>
    </svg>
  </section><!-- End Hero -->


  <div class="container contact-us-container" style="margin-bottom: 80px;" id="gotofirstpage">
    <h4 class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="margin-bottom: 50px;font-family: 'Poppins', sans-serif;font-weight:700;font-size:40px;color:#0078c8">Let's  <span style="color:#82be00">Connect</span></h4>
    <div class="row justify-content-center">
        <div class="col-xl-5 col-12">
            <h2 class="wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-family: 'Poppins', sans-serif;font-weight:700">Careers <span>at Infinity</span></h2>
            <p class="mb-5 pe-5 wow animate__animated animate__fadeInUp animate__delay-0.5s"> If you are interested in joining our award-winning team, please visit the <a href="{{route('join_infinity')}}" style="font-weight: 600;color:#0078C8"> Join Our Team </a>  to find a job that best suits you.</p>

            <h2 class="wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-family: 'Poppins', sans-serif;font-weight:700">Headquarters</h2>
            <h4 class="wow animate__animated animate__fadeInUp animate__delay-0.5s">Location:</h4>
            <p class="mb-0 wow animate__animated animate__fadeInUp animate__delay-0.5s"> 4700 Tama St. SE, Cedar Rapids, IA 52403</p>
            <p class="wow animate__animated animate__fadeInUp animate__delay-0.5s">30800 Telegraph Rd. Suite 4800, Bingham Farms, MI 48025</p>
         
            <h5 class="wow animate__animated animate__fadeInUp animate__delay-0.5s">Phone:</h4>
            <p class="wow animate__animated animate__fadeInUp animate__delay-0.5s">US: +1 800 341 0802 </p>
     
            <h5 class="wow animate__animated animate__fadeInUp animate__delay-0.5s" style="margin-bottom: 0px;">For current employee questions <br>or employment verification inquiries </h5>
            <p class="wow animate__animated animate__fadeInUp animate__delay-0.5s"  style="color:#0078C8;"><a href="mailto:hr@infinitydelivers.com" style="color:#0078C8">hr@infinitydelivers.com</a></p>

            <h5 class="wow animate__animated animate__fadeInUp animate__delay-0.5s" style="margin-bottom: 0px;">For career opportunities, <br> please visit the<a href="{{route('join_infinity')}}" style="font-weight: 600;color:#0078C8"> Join Our Team </a> page or  </h5>
            <p class="wow animate__animated animate__fadeInUp animate__delay-0.5s"  style="color:#0078C8"><a href="mailto:recruiting@infinitydelivers.com" style="color:#0078C8">recruiting@infinitydelivers.com </a></p>

            <h4 class="wow animate__animated animate__fadeInUp animate__delay-0.5s" style="margin-bottom: 0px;">For media relation inquiries</h5>
            <p class="wow animate__animated animate__fadeInUp animate__delay-0.5s" style="color:#a8b7c0" ><a href="mailto:marketing@infinitydelivers.com" style="color:#0078C8">marketing@infinitydelivers.com </a></p>

            <h5 class="wow animate__animated animate__fadeInUp animate__delay-0.5s" style="margin-bottom: 0px;">For partnership and business <br> solution inquiries </h5>
            <p class="wow animate__animated animate__fadeInUp animate__delay-0.5s" style="color:#0078C8" ><a href="mailto:info@infinitydelivers.com" style="color:#0078C8">info@infinitydelivers.com</a></p>
 
            
        </div>

        <div class="col-xl-6 col-12">
            <div class="card contact-card shadow px-md-4 py-3 wow animate__animated animate__fadeInUp animate__delay-0.5s">
                <div class="card-body ">
                    <h4>Contact Us Form</h4>
                    <p>Please complete the form below, and someone will contact you as soon as possible. Thank you.</p>
                    <form action="{{route('send_email')}}" method="POST">
                      @csrf
                    <div class="mb-3">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" required>
                      </div>

                      <div class="mb-3">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" required>
                      </div>

                      <div class="mb-3">
                        <input type="number" class="form-control" id="contact" name="contact" placeholder="Contact Number" required>
                      </div>

                      <div class="mb-3">
                        <input type="text" class="form-control" id="company" name="company" placeholder="Company" required>
                      </div>
                      <div class="mb-3">
                        <select class="form-select" aria-label="Default select example" id="contact_description" name="contact_description">
                            <option selected>Please describe why you're contacting Infinity</option>
                            <option value=">Career Opportunities">Career Opportunities </option>
                            <option value="Employment Verification">Employment Verification </option>
                            <option value="Partner with Infinity">Partner with Infinity </option>
                            <option value="Media Inquiries">Media Inquiries </option>
                          </select>
                      </div>

                      <div class="mb-3">
                        <textarea class="form-control text-area" id="message" name="message" placeholder="Message" rows="4"></textarea>
                      </div>
                 
                      <p>To ensure the best experience with our website and services, make sure you read and agree to the following terms:</p>
                  
                      <div class="form-check mb-0">
                        <input class="form-check-input" type="checkbox"  id="checkbox1">
                        <label class="form-check-label" style="font-family: 'Roboto', sans-serif;font-weight:200;">
                            I agree to receive insightful and relevant information.
                        </label>
                      </div>

                      <div class="form-check mb-4">
                        <input class="form-check-input" type="checkbox"  id="checkbox2">
                        <label class="form-check-label" style="font-family: 'Roboto', sans-serif;font-weight:200;">
                            I agree to have my personal data stored, processed and analyzed.
                        </label>
                      </div>

                      <div class="mb-3">
                        <button type="submit" disabled id="submit-contact" style="background-color:#9e9e9e;color:#fff;border-radius: 50px;border:none;" class="py-2 px-5 shadow ">Send</button>
                      </div>
                    </form>
                </div>
          
            </div>
        </div>

    </div>
  </div>
@endsection
@section('script')
  <script>
      $('input[type="checkbox"]').on('change',function(){
          var checkbox1 = $('#checkbox1').is(":checked");
          var checkbox2 = $('#checkbox2').is(":checked");
          checkbox1 && checkbox2  ? $('#submit-contact').css('background-color', '#82BE00').prop('disabled', false) :  $('#submit-contact').css('background-color', '#9e9e9e').prop('disabled', true);
      })
  </script>
@endsection

