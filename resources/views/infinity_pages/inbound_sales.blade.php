@extends('app.apps')
@section('title')
      <title>Inbound|infinity</title> 
@endsection
@section('content')
     <!-- ======= Hero Section ======= -->
  <section id="inbound-sales" style="margin-bottom: 80px;">
    <div class="container">
      <div class="row d-flex align-items-center">
        <div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1  hero-margin-desktop text-center">
          <div data-aos="zoom-out">
            <h1>Inbound Sales</span></h1>
            <h2>Dedicated Account Executives, with the right industry experience and demeanor
              to be successful, <br>are the backbone of your inbound sales campaigns. Designed 
              as an extension of your business, <br> we help build the necessary and seamless
              touch points for your customers.</h2>
            <div class="text-center">
              <a href="#gotofirstpage" class="btn-get-started scrollto green-btn">Get Started</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
      <defs>
        <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
      </defs>
      <g class="wave1">
        <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
      </g>
      <g class="wave2">
        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
      </g>
      <g class="wave3">
        <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
      </g>
    </svg>
  </section><!-- End Hero -->

<div class="container mb-3" id="gotofirstpage">
  <p class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-family: 'Roboto', sans-serif;font-weight: 400;font-size:40px;color:#000;font-family: 'Poppins', sans-serif;font-weight:700">Inbound  <span style="color:#000">Routing</span> & KPIs</p>
    <div class="row">
        <div class="col-xl-12">
          <p class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-family: 'Roboto', sans-serif;font-weight: 300;">Our system is flexible and uses best-in-class methodologies and technology.</p>     
      
        </div>
      </div>
  </div>

  <div class="container" style="margin-bottom: 80px;">
    <div class="row justify-content-center mb-3">
      <div class="col-xl-2 mb-3 wow animate__animated animate__fadeInUp animate__delay-0.5s">
          <div class="card shadow" style="border:none;border-radius: 20px;">
            <div class="card-body">
              <div class="d-flex justify-content-center mb-3">
                <img src="{{asset('/img/inbound/call_queuing.png')}}" alt="Avatar" style="max-width:50%;" class="img-responsive">
              </div>
              <p class="text-center" style="font-weight:900">Call <br> Queuing</p>
            </div>
          </div>
      </div>
      <div class="col-xl-2 mb-3 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card shadow" style="border:none;border-radius: 20px;">
          <div class="card-body">
            <div class="d-flex justify-content-center mb-3">
              <img src="{{asset('/img/inbound/smart_call_routing.png')}}" alt="Avatar" style="max-width:50%;" class="img-responsive">
            </div>
            <p class="text-center" style="font-weight:900">Smart Call <br>Routing</p>
          </div>
        </div>
    </div>
    <div class="col-xl-2 mb-3 wow animate__animated animate__fadeInUp animate__delay-0.5s">
      <div class="card shadow" style="border:none;border-radius: 20px;">
        <div class="card-body">
          <div class="d-flex justify-content-center mb-3">
            <img src="{{asset('/img/inbound/call_monitoring.png')}}" alt="Avatar" style="max-width:50%;" class="img-responsive">
          </div>
          <p class="text-center" style="font-weight:900">Call <br> Monitoring</p>
        </div>
      </div>
  </div>
  <div class="col-xl-2 mb-3 wow animate__animated animate__fadeInUp animate__delay-0.5s">
    <div class="card shadow" style="border:none;border-radius: 20px;">
      <div class="card-body">
        <div class="d-flex justify-content-center mb-3">
          <img src="{{asset('/img/inbound/integration_with_crm_ticketing_system_etc.png')}}" alt="Avatar" style="max-width:50%;" class="img-responsive">
        </div>
        <p class="text-center" style="font-weight:900">Integration (CRM, Ticketing System)</p>
      </div>
    </div>
</div>
  </div>
</div>

<div class="container" style="margin-bottom: 80px;">
  <div class="row justify-content-center">
    <div class="col-xl-5 wow  animate__animated animate__fadeInRightBig">
      <img src="{{asset('/img/inbound/about_inbound_sales.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive"> 
    </div>
    <div class="col-xl-5 wow  animate__animated animate__fadeInLeftBig" >
        <p style="font-family: 'Roboto', sans-serif;font-weight:300">Infinity has expertise meeting and exceeding <br> expectations with all inbound KPIs including:</p>
        <ul style="font-family: 'Roboto', sans-serif;font-weight:300">
          <li>Service Level</li>
          <li>Average Speed Answer</li>
          <li>Average Handle Time</li>
          <li>Average Wait Time</li>
          <li>Abandoned Call Rate</li>
          <li>Average Time for Call Return</li>
          <li>First Call Resolution</li>
          <li>CSI & NPS</li>
          <li>Employee Satisfaction</li>
        </ul>
    </div>
  </div>
</div>


<div class="container" style="margin-bottom: 80px;">
  <p class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-size:40px;font-weight:400;color:#000;font-family: 'Poppins', sans-serif;font-weight:700">Inbound  <span style="color:#000">Experience</span> <span style="color:#000">& Results</span></p>    
   <div class="row justify-content-center">
    <div class="col-xl-8 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
      <div class="overlay-container-infinity">
        <img src="{{asset('/img/inbound/pexels-photo-3861958 1.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
           <div class="joblisting-overlay">       
           </div>
           <div class="joblisting-text">
            <h1 class="mb-0" style="font-family: 'Poppins', sans-serif;font-weight:700">Technology</h1>
          </div>            
        </div>   
     </div>  
  </div>
  <div class="row justify-content-center">
    <div class="col-xl-8 mb-3">
      <p class="text-center" style=" font-weight: 300;">Industry leading technology company that specializes in internet-related services and products, which include online advertising technologies, a search engine, cloud computing, software, and hardware.</p>
     </div>  
  </div>
  <div class="row justify-content-center">
    <div class="col-xl-4 mb-3">
      <p style=" font-weight: 900;">Infinity’s Solution:</p>
      <ul style="font-family: 'Roboto', sans-serif;font-weight:300">
        <li> Provided global concierge service</li>
        <li>Supported over 20 products & services offered by client</li>
        <li>Redirected leads to sales funnel or provided support</li>
      </ul>
     </div>  
     <div class="col-xl-4 mb-3">
      <p style=" font-weight: 900;">Infinity’s Results:</p>
      <ul style="font-family: 'Roboto', sans-serif;font-weight:300">
        <li>Answered over 2.3MM calls annually</li>
        <li>Generated $3MM in revenue annually</li>
        <li>CSI rating of 96.45%</li>
      </ul>
     </div>  
  </div>
</div>



<div class="container" style="margin-bottom: 80px;">
   <div class="row justify-content-center">
    <div class="col-xl-8 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
      <div class="overlay-container-infinity">
        <img src="{{asset('/img/inbound/pexels-photo-7821914 1.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
           <div class="joblisting-overlay1">       
           </div>
           <div class="joblisting-text">
            <h1 class="mb-0" style="font-family: 'Poppins', sans-serif;font-weight:700">Credit Reporting</h1>
          </div>            
        </div>   
     </div>  
  </div>
  <div class="row justify-content-center">
    <div class="col-xl-8 mb-3">
      <p class="text-center" style=" font-weight: 300;">One of the largest consumer credit reporting companies worldwide.</p>
     </div>  
  </div>
  <div class="row justify-content-center">
    <div class="col-xl-4 mb-3">
      <p style=" font-weight: 900;">Infinity’s Solution:</p>
      <ul style="font-family: 'Roboto', sans-serif;font-weight:300">
        <li>Retention and customer support for VHR (vehicle history report)</li>
        <li>Answered calls from auto dealers</li>
        <li>Seamless integration with client’s global services</li>
      </ul>
     </div>  
     <div class="col-xl-4 mb-3">
      <p style=" font-weight: 900;">Infinity’s Results:</p>
      <ul style="font-family: 'Roboto', sans-serif;font-weight:300">
        <li> Answered over 380,000 calls annually</li>
        <li>Successfully saved over $25MM in recurring revenue</li>
        <li>Generated $8.5MM in upsell revenue</li>
      </ul>
     </div>  
  </div>
</div>




<div class="container" style="margin-bottom: 80px;">
   <div class="row justify-content-center">
    <div class="col-xl-8 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
      <div class="overlay-container-infinity">
        <img src="{{asset('/img/inbound/pexels-photo-5835580 1.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
           <div class="joblisting-overlay">       
           </div>
           <div class="joblisting-text">
            <h1 class="mb-0" style="font-family: 'Poppins', sans-serif;font-weight:700">Roadside Assistance</h1>
          </div>            
        </div>   
     </div>  
  </div>
  <div class="row justify-content-center">
    <div class="col-xl-8 mb-3">
      <p class="text-center" style=" font-weight: 300;">One of the country’s largest full-service roadside assistance administrators.</p>
     </div>  
  </div>
  <div class="row justify-content-center">
    <div class="col-xl-4 mb-3">
      <p style=" font-weight: 900;">Infinity’s Solution:</p>
      <ul style="font-family: 'Roboto', sans-serif;font-weight:300">
        <li>Handled all call types, ranging from basic call transfer to full dispatch service</li>
        <li>Supporting national footprint</li>
      </ul>
     </div>  
     <div class="col-xl-4 mb-3">
      <p style=" font-weight: 900;">Infinity’s Results:</p>
      <ul style="font-family: 'Roboto', sans-serif;font-weight:300">
        <li>Increased call volume handling to over half a million calls per month </li>
        <li>Reduced AHT from 12 minutes to under 10 minutes</li>
        <li>Improved service level from 50% to over 80%</li>
      </ul>
     </div>  
  </div>
</div>



<div class="container" style="margin-bottom: 80px;">
   <div class="row justify-content-center">
    <div class="col-xl-8 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
      <div class="overlay-container-infinity">
        <img src="{{asset('/img/inbound/pexels-photo-265087 1.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
           <div class="joblisting-overlay1">       
           </div>
           <div class="joblisting-text">
            <h1 class="mb-0" style="font-family: 'Poppins', sans-serif;font-weight:700">Marketing</h1>
          </div>            
        </div>   
     </div>  
  </div>
  <div class="row justify-content-center">
    <div class="col-xl-8 mb-3">
      <p class="text-center" style=" font-weight: 300;">Leader in outcome-based marketing, partnering with the world’s top brands.</p>
     </div>  
  </div>
  <div class="row justify-content-center">
    <div class="col-xl-4 mb-3">
      <p style=" font-weight: 900;">Infinity’s Solution:</p>
      <ul style="font-family: 'Roboto', sans-serif;font-weight:300">
        <li>Handled over 35 unique clients and call types </li>
        <li>Increased headcount by over 300%, as a result of our performance</li>
      </ul>
     </div>  
     <div class="col-xl-4 mb-3">
      <p style=" font-weight: 900;">Infinity’s Results:</p>
      <ul style="font-family: 'Roboto', sans-serif;font-weight:300">
        <li>Answered over 750,000 calls annually</li>
        <li>Increased service level and CSAT KPIs </li>
        <li>Scheduled over 238,000 sales and service appointments</li>
      </ul>
     </div>  
  </div>
</div>


<div class="container"  style="margin-bottom:80px;">
  <p class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-size:40px;font-weight:400;color:#0078C8;font-family: 'Poppins', sans-serif;font-weight:700">Related <span style="color:#82BE00">Services</span></p>
  <p class="text-center mb-5 wow animate__animated animate__fadeInUp animate__delay-0.8s" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
  font-weight: 300;">In the age of the digital consumer, we help you pique interest, elevate experience, nurture leads, make sales, reduce churn and grow customer <br>brand affinity with our CX driven engagement services and solutions – that support throughout the entire customer lifecycle.</p>           
  <div class="row justify-content-center">
      <div class="col-xl-3 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
        <div class="custom_overlay">
          <img src="{{asset('img/outbound 3.png')}}" class="img-fluid" style="max-width: 100%;  border-radius:10px;">   
              <a href="{{route('outbound_sales')}}">
                <div class="overlay">                
                  <div class="text">
                    <p id="solutions-title">Outbound Sales</p>
                    <a href="{{route('outbound_sales')}}" style="background-color:#82BE00;color:#fff;border-radius: 50px;" class="py-2 px-3">Learn More</a>
                  </div> 
                </div>
              </a>
          </div>
      </div>
     <div class="col-xl-3 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
       <div class="custom_overlay">
        <img src="{{asset('img/customer 2.png')}}" class="img-fluid" style="max-width: 100%; border-radius:10px;">   
          <a href="{{route('customer_service')}}">
            <div class="overlay">                
              <div class="text">
                <p id="solutions-title">Customer Service</p>
                <a href="{{route('customer_service')}}" style="background-color:#82BE00;color:#fff;border-radius: 50px;" class="py-2 px-3">Learn More</a>
              </div> 
              
            </div>
          </a>
       </div>   
      </div>
    </div>
  </div>




@endsection

