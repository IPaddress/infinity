@extends('app.apps')
@section('title')
<title>Our Team|infinity</title> 
@endsection
@section('content')
    <!-- ======= Hero Section ======= -->
    <section id="meet-our-team" style="margin-bottom: 80px;">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div
                    class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1  hero-margin-desktop text-center">
                    <div data-aos="zoom-out">
                        <h1>Meet Our Team</h1>
                        <h2>Meet the innovators at the helm of our success. Their diverse experience guides us in <br> delivering defining services to our clients, employees, and stakeholders alike.</h2>
                        <div class="text-center">
                            <a href="#gotostartpage" class="btn-get-started scrollto green-btn">About Infinity</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
            viewBox="0 24 150 28 " preserveAspectRatio="none">
            <defs>
                <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
            </defs>
            <g class="wave1">
                <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
            </g>
            <g class="wave2">
                <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
            </g>
            <g class="wave3">
                <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
            </g>
        </svg>
    </section><!-- End Hero -->

    <div class="container our-team-container" id="gotostartpage">
        <h2 class="text-center" style="margin-bottom:20px;font-family: 'Poppins', sans-serif;font-weight:700">Our <span>Story</span></h2>
        <p class="text-center px-md-5 mx-md-5" style="margin-bottom: 10px;">Infinity is an inside sales and customer service agency founded by INNOVATION and grown through PILOTS. <br>  We support our clients through the entire customer lifecycle – from lead generation and acquisition, <br> through customer service and retention.</p>
        <p class="text-center px-md-5 mx-md-5" style="margin-bottom: 80px;"> We take a highly-consultative approach with our clients to build, test and prove out successful inside sales <br>programs that go beyond the current revenue targets to achieve a New 100%. </p>
            <div class="row justify-content-center">
                <div class="col-xl-5 ps-0 pe-0">
                  <img src="{{ asset('/img/our_mission.png') }}" alt="Avatar" style="max-width:100%;border-bottom-left-radius:20px;border-top-left-radius: 20px;"
                        class="img-responsive">
                </div>
                <div class="col-xl-5 card-infinity-story p-0 ps-5">
                    <h3 class="mb-0">Our Mission</h3>
                    <p class="mb-0" style="font-size: 20px;">“Get Customers and Keep Customers”.</p>
                    <p class="mb-3"> We partner with some of the world’s  most admired <br>brands to help them  “Get Customers and Keep <br> Customers”.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="container " style="margin-bottom: 80px;">
        <div class="row justify-content-center">
            <div class="col-xl-11">
                <div class="main-timeline  ">
                    <div class="timeline">
                            <div class="timeline-year">
                            </div>
                            <div class="inner-content me-md-5">
                                <div class="body shadow wow animate__animated animate__fadeInUp animate__delay-0.5s" style="border-radius: 20px;">
                                    <div class="card-body ">
                                        <div class="row">
                                            <div class="col-md-8" style="border-right: 1px solid #D0D0D0">
                                                <p class="mb-0" style="color:#000">Founded company as a direct mail</p>
                                                <p style="color:#000">marketing services agency.</p>
                                                <img src="{{ asset('/img/Milestone/1996/2.png') }}" alt="Avatar"
                                                    style="max-width:100%;" class="img-responsive">
                                                <img src="{{ asset('/img/Milestone/1996/3.png') }}" alt="Avatar"
                                                    style="max-width:100%;" class="img-responsive">
                                            </div>
                                            <div class="col-md-4">
                                                <p class="mb-0 text-center"
                                                    style="color:#000;font-weight:900;font-size:35px;margin-top:45px;">
                                                    1996</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="timeline">
                            <div class="timeline-year">
                            </div>
                            <div class="inner-content  ms-md-5">
                                <div class="body shadow wow animate__animated animate__fadeInUp animate__delay-0.5s" style="border-radius: 20px;">
                                    <div class="card-body">
                                        <div class="row mb-3">
                                            <div class="col-md-8 " style="border-right: 1px solid #D0D0D0">
                                                <p style="color:#000">Added teleservices capabilities.</p>
                                                <img src="{{ asset('/img/Milestone/2001/1.png') }}" alt="Avatar"
                                                    style="max-width:100%;" class="img-responsive">
                                            </div>
                                            <div class="col-md-4">
                                                <p class="mb-0 text-center"
                                                    style="color:#000;font-weight:900;font-size:35px;margin-top:45px;">
                                                    2001</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="timeline">
                            <div class="timeline-year">
                            </div>
                            <div class="inner-content me-md-5">
                                <div class="body shadow wow animate__animated animate__fadeInUp animate__delay-0.5s" style="border-radius: 20px;">
                                    <div class="card-body">
                                        <div class="row py-4">
                                            <div class="col-md-2 mt-3">
                                                <img src="{{ asset('/img/Milestone/2003/1.png') }}" alt="Avatar"
                                                    style="max-width:100%;" class="img-responsive">
                                            </div>
                                            <div class="col-md-6 mt-3" style="border-right: 1px solid #D0D0D0">
                                                <p class="mb-0" style="color:#000">Partnered with first B2B </p>
                                                <p style="color:#000">client, still a client today.</p>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="mb-0 text-center"
                                                    style="color:#000;font-weight:900;font-size:35px;margin-top:20px;">
                                                    2004</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="timeline">
                            <div class="timeline-year">
                            </div>
                            <div class="inner-content ms-md-5">
                                <div class="body shadow wow animate__animated animate__fadeInUp animate__delay-0.5s"  style="border-radius: 20px;">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-8" style="border-right: 1px solid #D0D0D0">
                                                <p class="mb-0" style="color:#000">Built a B2B-focused model for client
                                                </p>
                                                <p style="color:#000">campaigns</p>
                                                <img src="{{ asset('/img/Milestone/2009/1.png') }}" alt="Avatar"
                                                    style="max-width:100%;" class="img-responsive">
                                                <img src="{{ asset('/img/Milestone/2009/2.png') }}" alt="Avatar"
                                                    style="max-width:100%;" class="img-responsive">
                                            </div>
                                            <div class="col-md-4">
                                                <p class="mb-0 text-center"
                                                    style="color:#000;font-weight:900;font-size:35px;margin-top:45px;">
                                                    2009</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="timeline">
                            <div class="timeline-year">
                            </div>
                            <div class="inner-content me-md-5">
                                <div class="body shadow wow animate__animated animate__fadeInUp animate__delay-0.5s" style="border-radius: 20px;">
                                    <div class="card-body">
                                        <div class="row py-4">
                                            <div class="col-md-2">
                                                <img src="{{ asset('/img/Milestone/2012/1.png') }}" alt="Avatar"
                                                    style="max-width:100%;" class="img-responsive">
                                            </div>
                                            <div class="col-md-6 mt-1" style="border-right: 1px solid #D0D0D0">
                                                <p class="mb-0 "  style="color:#000;">Launched operations in </p>
                                                <p style="color:#000;">Michigan</p>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="mb-0 text-center"
                                                    style="color:#000;font-weight:900;font-size:35px;margin-top:20px;">
                                                    2012</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="timeline">
                            <div class="timeline-year">
                            </div>
                            <div class="inner-content ms-md-5">
                                <div class="body shadow wow animate__animated animate__fadeInUp animate__delay-0.5s" style="border-radius: 20px;">
                                    <div class="card-body">
                                        <div class="row py-2">
                                            <div class="col-md-4">
                                                <img src="{{ asset('/img/Milestone/2014/1.png') }}" alt="Avatar"
                                                    style="max-width:100%;" class="img-responsive">
                                            </div>
                                            <div class="col-md-4 mt-3" style="border-right: 1px solid #D0D0D0">
                                                <p class="mb-0 "  style="color:#000;">Trademarked </p>
                                                <p style="color:#000;">Buyerlytics®</p>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="mb-0 text-center"
                                                    style="color:#000;font-weight:900;font-size:35px;margin-top:20px;">
                                                    2014</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="timeline">
                            <div class="timeline-year">
                            </div>
                            <div class="inner-content me-md-5">
                                <div class="body shadow wow animate__animated animate__fadeInUp animate__delay-0.5s" style="border-radius: 20px;">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-8" style="border-right: 1px solid #D0D0D0">
                                                <p class="mb-0" style="color:#000">Opened state-of-the-art facility in</p>
                                                <p style="color:#000">Bingham Farms, MI.</p>
                                                <img src="{{ asset('/img/Milestone/2018/1.png') }}" alt="Avatar"
                                                    style="max-width:100%;" class="img-responsive">
                                                <img src="{{ asset('/img/Milestone/2018/2.png') }}" alt="Avatar"
                                                    style="max-width:100%;" class="img-responsive">
                                                <img src="{{ asset('/img/Milestone/2018/3.png') }}" alt="Avatar"
                                                    style="max-width:100%;" class="img-responsive">
                                            </div>
                                            <div class="col-md-4">
                                                <p class="mb-0 text-center"
                                                    style="color:#000;font-weight:900;font-size:35px;margin-top:45px;">
                                                    2018</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="timeline">
                            <div class="timeline-year">
                            </div>
                            <div class="inner-content ms-md-5">
                                <div class="body shadow wow animate__animated animate__fadeInUp animate__delay-0.5s" style="border-radius: 20px;">
                                    <div class="card-body">
                                        <div class="row py-4">
                                            <div class="col-md-2">
                                                <img src="{{ asset('/img/Milestone/2019/1.png') }}" alt="Avatar"
                                                    style="max-width:100%;" class="img-responsive">
                                            </div>
                                            <div class="col-md-6 mt-2" style="border-right: 1px solid #D0D0D0">
                                                <p class="mb-0" style="color:#0078C8">Productized Buyerlytics® </p>
                                                <p class="mb-0" style="color:#0078C8">+ Introduced the </p>
                                                <p style="color:#0078C8">Innovation Lab.</p>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="mb-0 text-center"
                                                    style="color:#000;font-weight:900;font-size:35px;margin-top:20px;">
                                                    2019</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <div class="container" style="margin-bottom:80px;">
        <p class="text-center mb-5" style="font-size:40px;font-weight:400;color:#0078C8;font-family: 'Poppins', sans-serif;font-weight:700">Infinity’s <span
                style="color:#82BE00">Leadership</span></p>
            <div class="row justify-content-center">
                <div class="col-xl-2 mb-5">
                    <img src="{{ asset('/img/Leadership/0.webp') }}" alt="Avatar" id="img-team-profile"
                        style="max-width:100%;border-radius:50%;"
                        class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#tomLeidigh">
                    <p class="text-center" style="font-weight: 900">Tom Leidigh</p>
                </div>
    
                <div class="col-xl-2 mb-5">
                    <img src="{{ asset('/img/Leadership/1.webp') }}" id="img-team-profile" alt="Avatar"
                        style="max-width:100%;border-radius:50%;"
                        class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#jayDavey">
                    <p class="text-center" style="font-weight: 900">Jay Davey</p>
                </div>
    
    
                <div class="col-xl-2 mb-5">
                    <img src="{{ asset('/img/Leadership/2.webp') }}" alt="Avatar" id="img-team-profile"
                     style="max-width:100%;border-radius:50%;"
                        class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#tyRhamy">
                    <p class="text-center" style="font-weight: 900">Ty Rhamy</p>
                </div>
                <div class="col-xl-2 mb-5">
                    <img src="{{ asset('/img/Leadership/3.webp') }}" alt="Avatar"  id="img-team-profile"
                     style="max-width:100%;border-radius:50%;"
                        class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#christineHaerich">
                    <p class="text-center" style="font-weight: 900">Christine Haerich</p>
                </div>
                <div class="col-xl-2 mb-5">
                    <img src="{{ asset('/img/Leadership/4.webp') }}" alt="Avatar" id="img-team-profile"
                    style="max-width:100%;border-radius:50%;"
                        class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#joePenkala">
                    <p class="text-center" style="font-weight: 900">Joe Penkala</p>
                </div>
                <div class="col-xl-2 mb-5">
                    <img src="{{ asset('/img/Leadership/5.webp') }}" alt="Avatar" id="img-team-profile"
                    style="max-width:100%;border-radius:50%;"
                        class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#zachCalef">
                    <p class="text-center" style="font-weight: 900">Zach Calef</p>
                </div>
            </div>

            <div class="row justify-content-center">
          
            <div class="col-xl-2 mb-5">
                <img src="{{ asset('/img/Leadership/6.webp') }}" alt="Avatar" id="img-team-profile"
                style="max-width:100%;border-radius:50%;"
                    class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#elenaBorrie">
                <p class="text-center" style="font-weight: 900">Elena Borrie</p>
            </div>
            <div class="col-xl-2 mb-5">
                <img src="{{ asset('/img/Leadership/7.webp') }}" alt="Avatar" id="img-team-profile"
                style="max-width:100%;border-radius:50%;"
                    class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#feralDabre">
                <p class="text-center" style="font-weight: 900">Feral Dabre</p>
            </div>

            
            <div class="col-xl-2 mb-5">
                <img src="{{ asset('/img/Leadership/13.webp') }}" alt="Avatar" id="img-team-profile" 
                style="max-width:100%;border-radius:50%;"
                    class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#joe">
                <p class="text-center" style="font-weight: 900">Ryan McDonald</p>
            </div>

            <div class="col-xl-2 mb-5">
                <img src="{{ asset('/img/Leadership/11.webp') }}" alt="Avatar" id="img-team-profile"
                style="max-width:100%;border-radius:50%;"
                    class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#joshHalverson">
                <p class="text-center" style="font-weight: 900">Josh Halverson</p>
            </div>
   
            <div class="col-xl-2 mb-5">
                <img src="{{ asset('/img/Leadership/10.webp') }}" alt="Avatar" id="img-team-profile"
                style="max-width:100%;border-radius:50%;"
                    class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#tiffanyMoceri">
                <p class="text-center" style="font-weight: 900">Tiffany Moceri</p>
            </div>
       
            </div>

        <div class="row justify-content-center">
     
       
            <div class="col-xl-2 mb-5">
                <img src="{{ asset('/img/Leadership/9.webp') }}" alt="Avatar" id="img-team-profile"
                style="max-width:100%;border-radius:50%;"
                    class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#gregBest">
                <p class="text-center" style="font-weight: 900">Greg Best</p>
            </div>

            <div class="col-xl-2 mb-5">
                <img src="{{ asset('/img/Leadership/8.webp') }}" alt="Avatar" id="img-team-profile"
                style="max-width:100%;border-radius:50%;"
                    class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#barbaraHogan">
                <p class="text-center" style="font-weight: 900">Barbara Hogan</p>
            </div>



            <div class="col-xl-2 mb-5">
                <img src="{{ asset('/img/Leadership/13_new.jpg') }}" alt="Avatar" id="img-team-profile"
                style="max-width:100%;border-radius:50%;"
                    class="img-responsive mb-3" data-bs-toggle="modal" data-bs-target="#gabrielle">
                <p class="text-center" style="font-weight: 900">Gabrielle Appelhans</p>
            </div>


        </div>
    </div>

    <!-- Team Modals -->

    <div class="modal fade" id="tomLeidigh" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" style="width:63%;">
            <div class="modal-content" style="border-radius: 10px;">
                <div class="row justify-content-end">
                    <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                        <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-6">
                                <img src="{{ asset('/img/Leadership/0.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                            </div>
                            <div class="col-xl-6">
                              <p style="font-size:30px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Tom Leidigh, <span style="color:#82BE00">CEO</span></p>
                                <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                                  As Chief Executive Officer, Founder, and Owner, Tom guides the vision, mission, 
                                  and strategic thinking of Infinity. He has over 25+ years of executive experience 
                                  in sales, direct marketing, and program development. Tom is recognized as a leader
                                   in inside sales execution and methodologies and is primarily focused on industry 
                                   leadership, expansion strategies, channel relationships, and new company capabilities. 
                                   Tom was named one of the TOP 25 Influencers in the inside sales industry by the American 
                                   Association of Inside Sales Professionals in 2017 and 2018 and Innovator of the year for 2017.
                                    Tom earned an MBA with top honors from Arizona State University, and a BBA from the University of Iowa.
                                  </p>
                                <a href="https://www.linkedin.com/in/thomasleidigh/" class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="jayDavey" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl" style="width:63%;">
          <div class="modal-content" style="border-radius: 10px;">
              <div class="row justify-content-end">
                  <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                      <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
                  </div>
              </div>
              <div class="modal-body">
                  <div class="container">
                      <div class="row">
                          <div class="col-xl-6">
                              <img src="{{ asset('/img/Leadership/1.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                          </div>
                          <div class="col-xl-6">
                            <p style="font-size:30px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Jay Davey, <span style="color:#82BE00">President</span></p>
                              <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                                As President, Jay is responsible for providing strategic 
                                leadership and clear direction to ensure company alignment and growth. 
                                As the company continues to innovate in the sales industry, Jay works closely
                                with the CEO to provide the long-term vision and direction of Infinity. Jay assures
                                that all business units are aligned to provide the best results for Infinity’s clients,
                                with an eye on continued growth and innovation. Jay has over 20 years in sales and management
                                experience working with Fortune 500 companies. Prior to Infinity, Jay held various 
                                leadership roles within the automotive and telecommunications industries. Jay earned a BS
                                in Journalism from Michigan State University.
                              </p>
                              <a href="https://www.linkedin.com/in/jay-davey-12b87412/" class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>


  <div class="modal fade" id="tyRhamy" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" style="width:65%;">
        <div class="modal-content" style="border-radius: 10px;">
            <div class="row justify-content-end">
                <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                    <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6">
                            <img src="{{ asset('/img/Leadership/2.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                        </div>
                        <div class="col-xl-6">
                          <p style="font-size:25px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Ty Rhamy, <span style="color:#82BE00">VP of Corporate Development</span></p>
                            <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                              As Vice President of Corporate Developement, Ty oversees the formation 
                              and implementation of all new programs and client relationships at
                              Infinity. He works closely with the Executive & Leadership Teams 
                              to ensure business scalability, process efficiencies, and sales
                              enablement through Infinity’s Buyerlytics® Revenue System. Ty
                              started with Infinity in 2016 as an Account Executive. He has 
                              since grown in the company and held roles as an Assistant Sales Manager,
                              Sales Manager, Director, and now Vice President. Ty uses this experience and
                              perspective to create tactical and strategic alignment within all cross-functional
                              departments at Infinity. Prior to his career with Infinity, Ty worked in the automotive
                              ndustry after earning his BBA in Finance from The University of Iowa in 2013.
                            </p>
                            <a href="https://www.linkedin.com/in/tyrhamy/" class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="christineHaerich" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" style="width:60%;">
      <div class="modal-content" style="border-radius: 10px;">
          <div class="row justify-content-end">
              <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                  <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
              </div>
          </div>
          <div class="modal-body">
              <div class="container">
                  <div class="row">
                      <div class="col-xl-6">
                          <img src="{{ asset('/img/Leadership/3.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                      </div>
                      <div class="col-xl-6">
                        <p style="font-size:25px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Christine Haerich, <span style="color:#82BE00">Director of Business Development</span></p>
                          <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                            As Director of Business Development, Chris works closely with cross-functional
                            members of management to develop, coordinate and implement plans designed to 
                            increase existing business, capture new opportunities and enhance customer 
                            satisfaction. With over 25 years of leadership in client relations, operations, 
                            and compliance, Chris has a proven track record of the development and management
                            of innovative, complex partnerships, resulting in service excellence. Chris earned
                            her BA in English, from Northern State University, in Aberdeen, South Dakota.
                          </p>
                          <a href="https://www.linkedin.com/in/chris-haerich-95ba841/" class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>


<div class="modal fade" id="joePenkala" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" style="width:60%;">
      <div class="modal-content" style="border-radius: 10px;">
          <div class="row justify-content-end">
              <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                  <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
              </div>
          </div>
          <div class="modal-body">
              <div class="container">
                  <div class="row">
                      <div class="col-xl-6">
                          <img src="{{ asset('/img/Leadership/4.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                      </div>
                      <div class="col-xl-6">
                        <p style="font-size:25px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Joe Penkala, <span style="color:#82BE00"> Director of Agency and Client Engagement</span></p>
                          <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                              As Director of Agency and Client Engagement, Joe oversees 
                              all programs housed in the Bingham Farms location. He also 
                              works closely with our partners to ensure we have a shared
                              vision of success on current programs and collaboration
                              on future growth. Joe has used his 15 years of sales and
                              management experience to help build high performing sales
                              teams and develop sales leaders. Prior to Infinity, Joe spent time
                              in marketing and advertising, as well as in the recruiting industry.
                              Joe earned a BS in Sociology from St. Vincent College.
                          </p>
                          <a href="https://www.linkedin.com/in/joseph-penkala-169a363/" class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="zachCalef" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" style="width:67%;">
      <div class="modal-content" style="border-radius: 10px;">
          <div class="row justify-content-end">
              <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                  <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
              </div>
          </div>
          <div class="modal-body">
              <div class="container">
                  <div class="row">
                      <div class="col-xl-6">
                          <img src="{{ asset('/img/Leadership/5.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                      </div>
                      <div class="col-xl-6">
                        <p style="font-size:25px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Zach Calef, <span style="color:#82BE00"> Director of Agency and Client Engagement</span></p>
                          <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                              As the Director of Agency and Client Engagement, Zach is responsible for operational
                              and execution aspects of the sales and service programs in our Cedar Rapids agency.
                              He also leads all efforts regarding client relationships, focusing on delivering
                              high-quality performance to ensure strong partnerships between Infinity and the
                              clients we partner with. Zach works cross functionally with all departments and
                              utilizes his strong financial background and over 20 years of sales leadership to
                              drive growth for Infinity and our clients. Zach joined Infinity in 2015 as a sales
                              Manager, running our pilot programs, and began overseeing the agency in 2018. Prior
                              to his career with Infinity Zach held several management positions in the retail and
                              automotive and industries
                          </p>
                          <a href="https://www.linkedin.com/in/zach-calef-247b32a6/  " class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>


<div class="modal fade" id="elenaBorrie" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" style="width:67%;">
      <div class="modal-content" style="border-radius: 10px;">
          <div class="row justify-content-end">
              <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                  <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
              </div>
          </div>
          <div class="modal-body">
              <div class="container">
                  <div class="row">
                      <div class="col-xl-6">
                          <img src="{{ asset('/img/Leadership/6.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                      </div>
                      <div class="col-xl-6">
                        <p style="font-size:25px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Elena Borrie, <span style="color:#82BE00"> Director of Human Talent and Development</span></p>
                          <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                            As Director of Human Talent and Development, Elena’s primary focus is on developing and enforcing Infinity's HR goals, policies, and strategies. She develops strategies by identifying and researching human resources issues; contributing information, analysis, and establishes human resources objectives, that are representative of Infinity’s organizational objectives. Elena also plans, directs, and manages all human resource initiatives and employee relations, while ensuring fair employment practices, advising management on employee and labor policies and certifying employees' adherence to Infinity’s policies and procedures.
                            Prior to joining the Infinity team, Elena worked in Employee Services and Human Resources, at the Detroit Metro Airport.
                            Elena received her B.S. from Michigan State University.
                          </p>
                          <a href="https://www.linkedin.com/in/elena-borrie-03a523189/" class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>


<div class="modal fade" id="feralDabre" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" style="width:67%;">
      <div class="modal-content" style="border-radius: 10px;">
          <div class="row justify-content-end">
              <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                  <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
              </div>
          </div>
          <div class="modal-body">
              <div class="container">
                  <div class="row">
                      <div class="col-xl-6">
                          <img src="{{ asset('/img/Leadership/7.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                      </div>
                      <div class="col-xl-6">
                        <p style="font-size:25px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Feral Dabre, <span style="color:#82BE00"> Director of Analytics </span></p>
                          <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                            As Director of Analytics at Infinity, Feral has established a strong foundation of accuracy, integrity,
                            and transparency. Feral helps define and design automated and interactive reporting to provide a real-time
                            view of program and agent productivity. He fosters cross-functional collaboration by aligning analytics
                            strategies with the goals and capabilities of other internal departments. Feral has over 15 years of
                            experience in the field of Business Intelligence and Analytics working with a wide range of industries
                            from Banking, Finance, Marketing, Freight, Telecom, and Fleet management amongst others. Over the years
                            he has worked with business leaders and executives to identify and prioritize opportunities where data
                            and insights can provide significant value. He has a deep understanding of business operations and 
                            identifies paths to success. Feral has earned his BS in Chemistry from Mumbai University, India. 
                          </p>
                          <a href="https://www.linkedin.com/in/feral-a-dabre/" class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="barbaraHogan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" style="width:57%;">
      <div class="modal-content" style="border-radius: 10px;">
          <div class="row justify-content-end">
              <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                  <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
              </div>
          </div>
          <div class="modal-body">
              <div class="container">
                  <div class="row">
                      <div class="col-xl-6">
                          <img src="{{ asset('/img/Leadership/8.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                      </div>
                      <div class="col-xl-6">
                        <p style="font-size:25px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Barbara Hogan, <span style="color:#82BE00"> Finance Manager </span></p>
                          <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                          As Infinity’s Finance Manager, Barbara is responsible for overseeing all company operating expenses and cash flow, monitoring all financial details, and preparing financial statements and business activity reports. With more than 15-years of experience, she has also had careers within sales, medical, manufacturing, and the food industry. Barbara has a BA in Accounting, from Ashford University, as well as her MBA, specializing in Human Resource Management from Ashford University.
                          </p>
                          <a href="https://www.linkedin.com/in/barbara-hogan-27235a4a/" class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="gregBest" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" style="width:67%;">
        <div class="modal-content" style="border-radius: 10px;">
            <div class="row justify-content-end">
                <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                    <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6">
                            <img src="{{ asset('/img/Leadership/9.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                        </div>
                        <div class="col-xl-6">
                          <p style="font-size:25px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Greg Best, <span style="color:#82BE00"> Chief Compliance Officer & Senior Software Engineer  </span></p>
                            <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                              As Chief Compliance Officer, Greg oversees operational and technical compliance at Infinity.
                              As Senior Software Engineer, Greg is responsible for the development and support of Infinity’s
                              portfolio of business applications. Greg has more than 40 years of experience in information
                              technology, project management, process engineering, business and technical strategy,
                              data architecture, business analysis, systems design, software development, and business management.
                              He also holds certification in business process engineering and project management.
                              His work experience spans a wide range of industries such as insurance services,
                              environmental services, aerospace, customer contact, and telecommunications. Greg earned a BA in Chemistry from Amherst College. 
                            </p>
                            <a href="https://www.linkedin.com/in/gabest/" class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>


  <div class="modal fade" id="tiffanyMoceri" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" style="width:70%;">
        <div class="modal-content" style="border-radius: 10px;">
            <div class="row justify-content-end">
                <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                    <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6">
                            <img src="{{ asset('/img/Leadership/10.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                        </div>
                        <div class="col-xl-6">
                          <p style="font-size:25px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Tiffany Moceri, <span style="color:#82BE00"> Director of Agency Operations </span></p>
                            <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                              As the Director of Agency Operations, Tiffany has a multi-faceted job in which she is responsible for Training & Development, new client implementation, as well as managing all aspects of Infinity’s Bingham Farms facility needs. Tiffany works cross-functionally with all areas of the organization to ensure that her team is providing relevant and timely training to all levels of the organization. One of her many responsibilities is working closely with our clients and her training and quality team to provide a world-class level of training to all new and existing clients. 
                              As part of her role, she is tasked with onboarding and implementing all new clients that start in our Bingham Farms, MI. location. This entails working at all levels of the organization and creating a process to provide a seamless launch for all new clients who choose to partner with Infinity.
                            </p>
                            <a href="https://www.linkedin.com/in/tsrose/" class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="modal fade" id="joshHalverson" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" style="width:55%;">
        <div class="modal-content" style="border-radius: 10px;">
            <div class="row justify-content-end">
                <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                    <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6">
                            <img src="{{ asset('/img/Leadership/11.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                        </div>
                        <div class="col-xl-6">
                          <p style="font-size:25px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Josh Halverson, <span style="color:#82BE00"> Director of IT  </span></p>
                            <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                              As Director of IT, Josh oversees strategic and tactical IT initiatives at Infinity. Josh has more than 20 years of experience in IT infrastructure, security, disaster recovery, networking, and architecting infrastructure solutions. Since starting at Infinity in 2005 Josh has been responsible for the planning, implementation, administration, and enhancement of all networks and systems supporting the Infinity business. He holds certifications with Cisco and Microsoft. 
                            </p>
                            <a href="https://www.linkedin.com/in/joshdhalverson/" class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="modal fade" id="gabrielle" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" style="width:55%;">
        <div class="modal-content" style="border-radius: 10px;">
            <div class="row justify-content-end">
                <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                    <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6">
                            <img src="{{ asset('/img/Leadership/12.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                        </div>
                        <div class="col-xl-6">
                          <p style="font-size:25px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Gabrielle Appelhans, <span style="color:#82BE00"> Marketing Coordinator   </span></p>
                            <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                              As Coordinator of Marketing, Gabby organizes strategic-planning, social media, brand values, along with internal and external Infinity communications. She works closely with the leadership team to continue to expand Infinity’s capabilities within marketing. With 4 years of marketing experience, Gabby worked previously in the transportation industry before joining Infinity. Gabby earned her BA from Mount Mercy University in 2018.  
                            </p>
                            <a href="https://www.linkedin.com/in/gappelhans/" class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="modal fade" id="joe" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" style="width:65%;">
        <div class="modal-content" style="border-radius: 10px;">
            <div class="row justify-content-end">
                <div style="width: 20px; height: 24px; border-radius: 50%; text-align: right; background: #1489EA; margin: 10px 21px 19px 16px; display: flex; justify-content: center; align-items: center">
                    <i class="fal fa-times" style="cursor:pointer; text-align: right; font-size: 16px; color: white" data-bs-dismiss="modal"></i>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6">
                            <img src="{{ asset('/img/Leadership/13.webp') }}" alt="Avatar" style="max-width:100%;border-radius:10px;" class="img-responsive mb-3">
                        </div>
                        <div class="col-xl-6">
                          <p style="font-size:25px;font-weight:700;color:#0078C8;font-family: 'Poppins', sans-serif;"> Ryan McDonald, <span style="color:#82BE00">  Director of Agency Operations  </span></p>
                            <p style="font-weight:300;text-align:justify;font-family: 'Poppins', sans-serif;font-size:15px;">
                                    As the Director of Agency Operations, Ryan oversees the training & development and quality assurance departmen, at infinity's Cedar Rapids location.
                                    He also and works cross-functionally with our Leader Teams and department heads on the project management, new program implementation, and facilities management.
                            </p>
                            <a href="https://www.linkedin.com/in/ryan-mcdonald-2019/" class="btn" style="background-color:#0078C8;color:#fff;font-family: 'Poppins', sans-serif;font-weight:700 "> Connect on LinkedIn</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>

  

   


    <div class="container case-study-and-culture">
        <p class=" mb-3 text-center"
            style="font-size:40px;font-weight:400;color:#0078C8; font-family: 'Roboto', sans-serif;font-family: 'Poppins', sans-serif;font-weight:700">Our<span
                style="color:#82BE00"> Partners</span></p>
        <div class="row text-center justify-content-center">
            <div class="col-md-8">
                {{-- <p>if you are an existing channel partner please visit our partner portal <a href="https://ecxperience.com/"><strong
                        style="color:#0078C8">here</strong></a> </p> --}}
                <p class="mb-0">Infinity’s strategic partner network combines the right experience, resources, and knowledge to deliver the most innovative solutions, and actionable insights. Together, we provide an endless range of solutions to help our clients achieve, and often exceed, their business goals.</p>
                <img src="{{ asset('/img/ecx_logo.png') }}" alt="Avatar" style="max-width:100%;"
                    class="img-responsive mb-3">
                <p><strong>EC Xperience </strong> is a next generation digital solutions that provides complete contact center operations and
                    infrastructure at Concepcion Baliuag, Bulacan Philippines, with capacity of 700+ seat and Antipolo City
                    with 200 seats. Each location offers complete amenities and facilities for client and employees’ needs.
                </p>
                <p>For more information about EC Xperience, please click here. <a href="https://ecxperience.com/"><strong style="color:#0078C8">here.</strong></a></p>
            </div>
        </div>
    </div>


@endsection