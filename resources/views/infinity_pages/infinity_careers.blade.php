@extends('app.apps')
@section('content')
     <!-- ======= Hero Section ======= -->
  <section id="join-infinity" style="margin-bottom: 80px;">
    <div class="container">
      <div class="row d-flex align-items-center">
        <div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1  hero-margin-desktop text-center">
          <div data-aos="zoom-out">
            <h1>Explore Infinity Careers</span></h1>
            <h2>We're always looking for creative, customer-centered people who love interacting with <br> customers. Our team members are motivated, smart, and passionate about using  <br> technology to serve others. </h2>
            <div class="text-center">
              <a href="#about" class="btn-get-started scrollto green-btn">Explore</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
      <defs>
        <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
      </defs>
      <g class="wave1">
        <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
      </g>
      <g class="wave2">
        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
      </g>
      <g class="wave3">
        <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
      </g>
    </svg>
  </section><!-- End Hero -->

  

  <div class="container infinity-careers">
    <h2 class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s">Michigan  <span>Headquarters</span></h2>
    <p class="text-center mb-0 wow animate__animated animate__fadeInUp animate__delay-0.5s" >Looking for a new and exciting opportunity to shine? </p>   
    <p class="text-center px-md-5 mx-md-5 wow animate__animated animate__fadeInUp animate__delay-0.5s" style="margin-bottom: 80px;">Interested working with us? We are too!</p>    
    <div class="row justify-content-center mb-5 wow animate__animated animate__fadeInUp animate__delay-0.5s">
      <div class="col-xl-4 mb-3">
       <div class="input-group ">
         <input type="text" class="form-control" placeholder="Job Title" aria-label="Recipient's username" aria-describedby="basic-addon2">
       </div>
      </div>
      <div class="col-xl-1 mb-3">
        <div class="d-grid gap-2">
          <a id="btn-search" class="btn-block" href="#">Search</a>
        </div>
        
      </div>
     </div>



     <div class="row">
  
      <div class="col-xl-3 mb-3 col-md-6 col-sm-6 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card card-job-available shadow">
          <div class="card-body text-center">
            <h1>Software Engineer</h1>
            <p class="mb-0">Full Time</p>
            <p>Bingham Farms, MI, US</p>
            <a href="#" class="btn shadow-sm" data-bs-toggle="modal" data-bs-target="#joblist">View Details  <i class="fal fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>

      <div class="col-xl-3 mb-3 col-md-6 col-sm-6 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card card-job-available shadow">
          <div class="card-body text-center">
            <h1>Front End Developer</h1>
            <p class="mb-0">Full Time</p>
            <p>Bingham Farms, MI, US</p>
            <a href="#" class="btn shadow-sm"  data-bs-toggle="modal" data-bs-target="#joblist">View Details  <i class="fal fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>

      <div class="col-xl-3  mb-3 col-md-6 col-sm-6 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card card-job-available shadow">
          <div class="card-body text-center">
            <h1>Back End Developer</h1>
            <p class="mb-0">Full Time</p>
            <p>Bingham Farms, MI, US</p>
            <a href="#" class="btn shadow-sm"  data-bs-toggle="modal" data-bs-target="#joblist">View Details  <i class="fal fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>

      <div class="col-xl-3 mb-3 col-md-6 col-sm-6 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card card-job-available shadow">
          <div class="card-body text-center">
            <h1>UI/UX Developer</h1>
            <p class="mb-0">Full Time</p>
            <p>Bingham Farms, MI, US</p>
            <a href="#" class="btn shadow-sm"  data-bs-toggle="modal" data-bs-target="#joblist">View Details  <i class="fal fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>


      <div class="col-xl-3 col-md-6 col-sm-6 mb-3 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card card-job-available shadow">
          <div class="card-body text-center">
            <h1>Call Centre Agent</h1>
            <p class="mb-0">Full Time</p>
            <p>Bingham Farms, MI, US</p>
            <a href="#" class="btn shadow-sm"  data-bs-toggle="modal" data-bs-target="#joblist">View Details  <i class="fal fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>

      <div class="col-xl-3 mb-3 col-md-6 col-sm-6 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card card-job-available shadow">
          <div class="card-body text-center">
            <h1>Call Centre Manager</h1>
            <p class="mb-0">Full Time</p>
            <p>Bingham Farms, MI, US</p>
            <a href="#" class="btn shadow-sm"  data-bs-toggle="modal" data-bs-target="#joblist">View Details  <i class="fal fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>

      <div class="col-xl-3 mb-3 col-md-6 col-sm-6 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card card-job-available shadow">
          <div class="card-body text-center">
            <h1>Resource Analyst</h1>
            <p class="mb-0">Full Time</p>
            <p>Bingham Farms, MI, US</p>
            <a href="#" class="btn shadow-sm"  data-bs-toggle="modal" data-bs-target="#joblist">View Details  <i class="fal fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>

      <div class="col-xl-3 mb-3 col-md-6 col-sm-6 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card card-job-available shadow">
          <div class="card-body text-center">
            <h1>Quality Analyst</h1>
            <p class="mb-0">Full Time</p>
            <p>Bingham Farms, MI, US</p>
            <a href="#" class="btn shadow-sm"  data-bs-toggle="modal" data-bs-target="#joblist">View Details  <i class="fal fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>

      <div class="col-xl-3 mb-3 col-md-6 col-sm-6 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card card-job-available shadow">
          <div class="card-body text-center">
            <h1>Programmer</h1>
            <p class="mb-0">Full Time</p>
            <p>Bingham Farms, MI, US</p>
            <a href="#" class="btn shadow-sm"  data-bs-toggle="modal" data-bs-target="#joblist">View Details  <i class="fal fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>

      <div class="col-xl-3 mb-3 col-md-6 col-sm-6 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card card-job-available shadow">
          <div class="card-body text-center">
            <h1>Human Resources</h1>
            <p class="mb-0">Full Time</p>
            <p>Bingham Farms, MI, US</p>
            <a href="#" class="btn shadow-sm"  data-bs-toggle="modal" data-bs-target="#joblist">View Details  <i class="fal fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>

      <div class="col-xl-3  mb-3 col-md-6 col-sm-6 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card card-job-available shadow">
          <div class="card-body text-center">
            <h1>Trainer</h1>
            <p class="mb-0">Full Time</p>
            <p>Bingham Farms, MI, US</p>
            <a href="#" class="btn shadow-sm"  data-bs-toggle="modal" data-bs-target="#joblist">View Details  <i class="fal fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>

      <div class="col-xl-3 mb-3 col-md-6 col-sm-6 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card card-job-available shadow">
          <div class="card-body text-center">
            <h1>Coach</h1>
            <p class="mb-0">Full Time</p>
            <p>Bingham Farms, MI, US</p>
            <a href="#" class="btn shadow-sm"  data-bs-toggle="modal" data-bs-target="#joblist">View Details  <i class="fal fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="joblist" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 10px;">
            <div class="row justify-content-end">
                <div
                    style="width: 30px; height: 30px; border-radius: 50%; text-align: right; background: #1489EA; margin: 25px 25px 0 25px; display: flex; justify-content: center; align-items: center">
                    <i class="fal fa-times fa-3x"
                        style="cursor:pointer; text-align: right; font-size: 25px; color: white"
                        data-bs-dismiss="modal"></i>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                      <div class="col-md-12">
                        <div id="iframeHeightDiv" name="HRM Direct Career Site iFrame Container" align="center">
                           <iframe id="inlineframe" name="HRM Direct Career Site iFrame" sandbox="allow-top-navigation allow-scripts allow-forms allow-popups allow-same-origin allow-popups-to-escape-sandbox" src="https://infinitydelivers.hrmdirect.com/employment/job-openings.php?search=true&" frameborder="0" allowtransparency="true" title="Career Site"> </iframe>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
<script type="text/javascript" src="//reports.hrmdirect.com/employment/default/sm/settings/dynamic-embed/dynamic-iframe-embed-js.php"></script> 
@endsection

