@extends('app.apps')
@section('title')
      <title>Solutions|infinity</title> 
@endsection
@section('content')
     <!-- ======= Hero Section ======= -->
  <section id="solutions" style="margin-bottom: 80px;">
    <div class="container">
      <div class="row  d-flex align-items-center">
        <div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1  hero-margin-desktop text-center">
          <div data-aos="zoom-out">
            <h1>Solutions</span></h1>
            <h2>Infinity is an inside sales and customer service agency, founded in 1996. We specialize in highly consultative,<br> innovative partnerships with our clients, who are some of the most admired brands. In a nutshell, we “Get <br>Customers and Keep Customers” for our clients</h2>
            <h2> Infinity delivers high-performance results through our proprietary revenue system methodology, Buyerlytics®. Buyerlytics® <br> integrates cross-functional components that are critical for inside sales and service success, <br> including Strategy, Systems, and Execution.</h2>
            <div class="text-center">
              <a href="#infinity-offers" class="btn-get-started scrollto green-btn">Get Started</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
      <defs>
        <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
      </defs>
      <g class="wave1">
        <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
      </g>
      <g class="wave2">
        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
      </g>
      <g class="wave3">
        <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
      </g>
    </svg>
  </section><!-- End Hero -->

  <div class="container" id="infinity-offers">
    <div class="row">
      <div class="col-lg-12 text-center wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <p style="font-family: 'Roboto', sans-serif;font-weight:400;font-size: 40px;color:#000;font-family: 'Poppins', sans-serif;font-weight:700">Infinity <span style="color:#000">Offers</span></p>
        <div class="row justify-content-center">
          <div class="col-xl-8 mx-3 col-12 wow animate__animated animate__fadeInUp animate__delay-0.5s">
            <div class="d-flex justify-content-center">
              <p class="text-center mb-3 wow animate__animated animate__fadeInUp animate__delay-0.5s"  style="font-family: 'Roboto', sans-serif;font-weight:300;">Today’s customers are connected like never before, communicating across
                multiple channels simultaneously, as they engage with your brand. Infinity
                brings the human element of conversations back to customer experience to
                improve the outcome of customer interactions, promote and protect your
                brand, and accelerate growth.</p>  
            </div> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container" style="margin-bottom: 80px;">
    <div class="row pt-4 justify-content-center">
        <div class="col-xl-3 mb-3 px-0 wow animate__animated animate__zoomIn animate__delay-0.8s">
          <div class="d-flex justify-content-center align-items-center">
            <div class="flip-card-solutions" style="cursor: context-menu">
                <div class="flip-card-inner">
                  <div class="flip-card-front"  style="border:1px solid #0078c8;border-radius:50%;">
                    <img src="{{asset('/img/What Infinity Offers/strategy.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
                  </div>
                </div>
              </div>
        </div>
        <p class="mb-2 text-center" style="font-size:24px;font-weight:900;margin-top:25px;">Strategy</p>
        <p class="text-center mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
        font-weight: 300;">Go-To-Market Plan</p>
        <p class="text-center mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
        font-weight: 300;">Revenue Plan &amp; ROI</p>
        <p class="text-center mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
        font-weight: 300;">Addressable Market</p>
        <p class="text-center mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
        font-weight: 300;">Organizational Alignment</p>
      </div>

     <div class="col-xl-3 mb-3 px-0 wow animate__animated animate__zoomIn animate__delay-0.8s">
        <div class="d-flex justify-content-center align-items-center">
          <div class="flip-card-solutions" style="cursor: context-menu">
              <div class="flip-card-inner">
                <div class="flip-card-front" style="border:1px solid #90d200;border-radius:50%;">
                  <img src="{{asset('/img/What Infinity Offers/system.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
                </div>
              </div>
            </div>
          </div>
          <p class="mb-2 text-center" style="font-size:24px;font-weight:900;margin-top:25px;">Systems</p>
          <p class="text-center mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
          font-weight: 300;">Sales Performance Analytics</p>
          <p class="text-center mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
          font-weight: 300;">Campaign Analytics</p>
          <p class="text-center mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
          font-weight: 300;">Customer Analytics</p>
          <p class="text-center mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
          font-weight: 300;">Support Systems</p>
          <p class="text-center mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
          font-weight: 300;">Compliance</p>
       </div>

    <div class="col-xl-3 mb-3  px-0 wow animate__animated animate__zoomIn animate__delay-0.8s">
      <div class="d-flex justify-content-center align-items-center">
        <div class="flip-card-solutions" style="cursor: context-menu">
            <div class="flip-card-inner">
              <div class="flip-card-front" style="border:1px solid #0078c8;border-radius:50%;">
                <img src="{{asset('/img/What Infinity Offers/execution.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive">
              </div>
            </div>
          </div>
        </div>
          <p class="mb-2 text-center" style="font-size:24px;font-weight:900;margin-top:25px;">Execution</p>
          <p class="text-center mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
          font-weight: 300;">Team Structure & Culture</p>
          <p class="text-center mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
          font-weight: 300;">Sales Process</p>
          <p class="text-center mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
          font-weight: 300;">Performance Management</p>
          <p class="text-center mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
          font-weight: 300;">Human Talent Training </p>
      </div>
  </div>
</div>


<div class="container" style="margin-bottom: 10px;">
  <p class="text-center" style="font-size: 40px;color:#000;margin-bottom:40px;font-family: 'Poppins', sans-serif;font-weight:700">Buyerlytics<span style="color:#000">® </span></p>
  <div class="row justify-content-center">
    <div class="col-lg-4 align-items-center px-4">
      <a href="" data-bs-toggle="modal" data-bs-target="#infinity" >
      <img src="{{asset('/img/buyerlytics.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive" >
    </a>
    </div>
    <div class="col-lg-6">
      <p class=" mb-3" style="font-size: 15px;  font-family: 'Roboto', sans-serif;
      font-weight: 300;">Infinity has an outstanding track record of delivering superior results to
      clients, stakeholders, employees, and partners.</p>
      <p class=" mb-3" style="font-size: 15px;  font-family: 'Roboto', sans-serif;
      font-weight: 300;">Our in-depth knowledge
      and experience, is the foundation for developing custom go-to-market
      strategies, for prospective clients.</p>
      <p class=" mb-3" style="font-size: 15px;  font-family: 'Roboto', sans-serif;
      font-weight: 300;"> 
      Our boutique approach offers a highly experienced management team is
      committed to exceeding our client&#39;s ROI targets. We take pride in our
      exceptional client references, as they are the greatest testament of our
      commitment to results.</p>
    
      </div>
  </div>
</div>


<!-- --------------------------------
Modal Video Infinity
----------------------------------->
<div class="modal fade" id="infinity" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="row justify-content-end">
              <div
                  style="width: 30px; height: 30px; border-radius: 50%; text-align: right; background: #1489EA; margin: 25px 25px 0 25px; display: flex; justify-content: center; align-items: center">
                  <i class="fal fa-times fa-3x"
                      style="cursor:pointer; text-align: right; font-size: 25px; color: white"
                      data-bs-dismiss="modal"></i>
              </div>
            </div>
          <div class="modal-body">
              <div class="container">
                  <div class="row justify-content-center">
                      <div class="col-xl-12 pb-5">
                        <video id="infinity-video" src="" loop autoplay style="max-width: 100%;"></video>
                        {{-- <img src="{{asset('/img/hero.gif')}}" id="solution-img" class="img-responsive" style="max-width:100%;"/> --}}
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-10 px-4" style="margin-top:20px;">
        <p class=" mb-2" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
        font-weight: 300;">Our experience spans the entire customer lifecycle (B2B & B2C) including:</p>
        <div class="row">
          <div class="col-md-5">
            <ul>
              <li> <p class=" mb-0" style="font-size: 15px;  font-family: 'Roboto', sans-serif;font-weight: 300;">	Outbound Acquisition Sales</p></li>
              <li> <p class=" mb-0" style="font-size: 15px;  font-family: 'Roboto', sans-serif;font-weight: 300;">	Pilots/Innovation Lab</p></li>
              <li> <p class=" mb-0" style="font-size: 15px;  font-family: 'Roboto', sans-serif;font-weight: 300;">	Customer Retention, Loyalty & Winback</p></li>
              <li> <p class=" mb-0" style="font-size: 15px;  font-family: 'Roboto', sans-serif;font-weight: 300;">	Appointment Setting/Scheduling</p></li>
            </ul>
          </div>
      
  
  
  
          <div class="col-md-5">
            <ul>
            <li> <p class=" mb-0" style="font-size: 15px;  font-family: 'Roboto', sans-serif;font-weight: 300;">	Customer Service</p></li>
            <li> <p class=" mb-0" style="font-size: 15px;  font-family: 'Roboto', sans-serif;font-weight: 300;">	Lead Generation</p></li>
            <li> <p class=" mb-0" style="font-size: 15px;  font-family: 'Roboto', sans-serif;font-weight: 300;">	Data Entry, Back Office</p></li>
          </ul>
          </div>
        </div>
     
        <p class=" mb-3" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
           font-weight: 300;">Infinity is a partner that’s wholly focused on your success and committed to exceeding your ROI targets. We are immersed in every step of the process, including recruiting the right employee profiles, training extensively in the specifics of your customer needs, and managing the results to ensure quality and productivity.
          </p>
          <p class=" mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
          font-weight: bold;">Outbound Sales </p> 
        <p class=" mb-3" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
           font-weight: 300;">Infinity’s B2B & B2C outbound sales programs drive results by using outcome-oriented KPIs and dedicated cross-functional teams. These programs are built with Account Executives that have a “hunter” sales mentality and a strong competitive nature. They are supported by cross-functional management focused on outcomes, not just effort.</p>
  
        <p class=" mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
        font-weight: bold;">Inbound Sales </p> 
        <p class=" mb-3" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
        font-weight: 300;">Dedicated Account Executives with the right industry experience and demeanor, will be successful as the backbone of your inbound B2B or B2C sales campaigns. Designed as an extension of your business, we help build the necessary and seamless touch points for your customers.
        </p>
  
        <p class=" mb-0" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
        font-weight: bold;">Customer Service </p> 
        <p class=" mb-3" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
        font-weight: 300;">Infinity’s B2B and B2C customer service campaigns, include loyalty programs, customer support, reminder series, surveys, and call overflow and backstopping. When your resources just aren’t enough, we’re here to fill in with the necessary support to help you keep up with volume and avoid inactive customers.
        </p>
    </div>
    </div>
    </div>
  </div>
   

<div class="container"  style="margin-bottom:80px;">
  <p class="text-center wow animate__animated animate__fadeInUp animate__delay-0.8s" style="font-size:40px;color:#0078C8;font-family: 'Poppins', sans-serif;font-weight:700">Our  <span style="color:#82BE00">Services</span></p>
  <p class="text-center mb-5 wow animate__animated animate__fadeInUp animate__delay-0.8s" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
  font-weight: 300;">In the age of the digital consumer, we help you pique interest, elevate experience, nurture leads, make sales, reduce churn and grow customer <br>brand affinity with our CX driven engagement services and solutions – that support throughout the entire customer lifecycle.</p>          
  <div class="row justify-content-center">
        <div class="col-xl-3 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
          <div class="custom_overlay">
            <img src="{{asset('img/inbound 1.png')}}" class="img-fluid" style="max-width: 100%;border-radius:10px;">    
                <a href="{{route('inbound_sales')}}">
                      <div class="overlay">                
                        <div class="text">
                          <p id="solutions-title">Inbound Sales</p>
                          <a href="{{route('inbound_sales')}}" style="background-color:#82BE00;color:#fff;border-radius: 50px;" class="py-2 px-3">Learn More</a>
                        </div> 
                    </div>
                </a>
           </div>
        </div>
      <div class="col-xl-3 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
        <div class="custom_overlay">
          <img src="{{asset('img/outbound 3.png')}}" class="img-fluid" style="max-width: 100%;  border-radius:10px;">   
              <a href="{{route('outbound_sales')}}">
                <div class="overlay">                
                  <div class="text">
                    <p id="solutions-title">Outbound Sales</p>
                    <a href="{{route('outbound_sales')}}" style="background-color:#82BE00;color:#fff;border-radius: 50px;" class="py-2 px-3">Learn More</a>
                  </div> 
                </div>
              </a>
          </div>
      </div>
     <div class="col-xl-3 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
       <div class="custom_overlay">
        <img src="{{asset('img/customer 2.png')}}" class="img-fluid" style="max-width: 100%; border-radius:10px;">   
          <a href="{{route('customer_service')}}">
            <div class="overlay">                
              <div class="text">
                <p id="solutions-title">Customer Service</p>
                <a href="{{route('customer_service')}}" style="background-color:#82BE00;color:#fff;border-radius: 50px;" class="py-2 px-3">Learn More</a>
              </div> 
              
            </div>
          </a>
       </div>   
      </div>
    </div>
  </div>



@endsection

@section('script')
    <script>
      $("#infinity").on("shown.bs.modal", function(e) {
        $("#infinity-video").attr(
          "src","{{asset('img/Infinity Delivers - Buyerlytics - Whiteboard Animation_720P HD.mp4')}}"
        );
      });

      $("#infinity").on("hide.bs.modal", function(e) {
        $("#infinity-video").attr("src", "");
      });


    </script>
@endsection