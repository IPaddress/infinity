@extends('app.apps')
  @section('title')
    <title>Outbound|infinity</title> 
  @endsection
@section('content')
     <!-- ======= Hero Section ======= -->
  <section id="outbound-sales" style="margin-bottom: 80px;">
    <div class="container">
      <div class="row d-flex align-items-center">
        <div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1  hero-margin-desktop text-center">
          <div data-aos="zoom-out">
            <h1>Outbound Sales</span></h1>
            <h2>Infinity’s outbound sales programs drive results by using outcome-oriented KPIs and <br> dedicated cross-functional teams. These programs are built with Account Executives <br>that have a “hunter” sales mentality and a strong competitive nature. They are <br> supported by cross-functional management focused on outcomes, not just effort.</h2>
            <div class="text-center">
              <a href="#outbound" class="btn-get-started scrollto green-btn">Get Started</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
      <defs>
        <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
      </defs>
      <g class="wave1">
        <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
      </g>
      <g class="wave2">
        <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
      </g>
      <g class="wave3">
        <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
      </g>
    </svg>
  </section><!-- End Hero -->

  <div class="container mb-3" id="outbound">
      <p class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-family: 'Roboto', sans-serif;font-size:40px;font-weight:400;color:#000;font-family: 'Poppins', sans-serif;font-weight:700">Outbound   <span style="color:#000">Capabilities</span></p>
      <p class="text-center px-md-5 mx-md-5 wow animate__animated animate__fadeInUp animate__delay-0.5s" style="margin-bottom:80px;font-family: 'Roboto', sans-serif;font-weight:300;">
        Infinity has mastered the art and science of selling. Infinity delivers high-performance results  through our proprietary <br> revenue  system methodology,
        Buyerlytics®. We work with our clients and utilize our methodologies to
        calculate a custom <br>revenue projection, based on your specific sales cycle,
        campaign type, and desired results. The outcome will be one <br>that is results
        driven, whether you are looking to drive  more sales revenue or simply offer a
        better customer experience.</p> 

      <div class="row justify-content-center" style="margin-bottom:80px;">
      <div class="col-xl-2 mb-3 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card shadow" style="border:none;border-radius: 20px;">
          <div class="card-body">
            <div class="d-flex justify-content-center mb-3">
              <img src="{{asset('/img/Outbound/acquisition_sales.png')}}" alt="Avatar" style="max-width:50%;" class="img-responsive">
            </div>
            <p class="text-center " style="font-weight:900">Acquisition <br> Sales</p>
          </div>
        </div>
      </div>

      <div class="col-xl-2 mb-3 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card shadow" style="border:none;border-radius: 20px;">
          <div class="card-body">
            <div class="d-flex justify-content-center mb-3">
              <img src="{{asset('/img/Outbound/lead_generation.png')}}" alt="Avatar" style="max-width:50%;" class="img-responsive">
            </div>
            <p class="text-center" style="font-weight:900">Lead <br> Generation</p>
          </div>
        </div>
      </div>

      <div class="col-xl-2 mb-3 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card shadow" style="border:none;border-radius: 20px;">
          <div class="card-body">
            <div class="d-flex justify-content-center mb-3">
              <img src="{{asset('/img/Outbound/customer_retention.png')}}" alt="Avatar" style="max-width:50%;" class="img-responsive">
            </div>
            <p class="text-center" style="font-weight:900">Customer <br> Retention</p>
          </div>
        </div>
      </div>

      <div class="col-xl-2 mb-3 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card shadow" style="border:none;border-radius: 20px;">
          <div class="card-body">
            <div class="d-flex justify-content-center mb-3">
              <img src="{{asset('/img/Outbound/customer_service.png')}}" alt="Avatar" style="max-width:50%;" class="img-responsive">
            </div>
            <p class="text-center" style="font-weight:900">Account <br> Management</p>
          </div>
        </div>
      </div>

      <div class="col-xl-2 mb-3 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card shadow" style="border:none;border-radius: 20px;">
          <div class="card-body">
            <div class="d-flex justify-content-center mb-3">
              <img src="{{asset('/img/Outbound/daata_entry-back_office.png')}}" alt="Avatar" style="max-width:50%;" class="img-responsive">
            </div>
            <p class="text-center" style="font-weight:900">Data Entry & <br> Back Office</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  

  <div class="container" style="margin-bottom: 80px;">
    <div class="row justify-content-center">
      <p class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s mb-3" style="font-family: 'Roboto', sans-serif;font-size:40px;font-weight:400;color:#0078C8;font-family: 'Poppins', sans-serif;font-weight:700">Our   <span style="color:#82BE00">Result</span></p>
      <div class="col-xl-5 wow  animate__animated animate__fadeInRightBig">
        <img src="{{asset('/img/Outbound/outbound.png')}}" alt="Avatar" style="max-width:100%;" class="img-responsive"> 
      </div>
      <div class="mt-5 col-xl-5 wow  animate__animated animate__fadeInLeftBig" >
          <p style="  font-family: 'Roboto', sans-serif;font-weight:300;">Infinity’s B2B/B2C Expertise</p>
          <ul style="  font-family: 'Roboto', sans-serif;font-weight:300;">
            <li>Over 15 years of sales experience</li>
            <li>Re-branded company in 2015 with focus on B2B, high level sales management.</li>
            <li>Recognized as the “Innovation Center” for the world’s leading digital marketing company. </li>
            <li>Have piloted over 20 programs in our history that have been replicated and scaled in multiple markets and countries.</li>
            <li>Engine behind CISO, the AA-ISP Inside Sales Certification process.</li>
          </ul>
      </div>
    </div>
  </div>


<div class="container" style="margin-bottom:80px;">
  <div class="row justify-content-center align-item-center">
    <div class="col-lg-12 pt-5 pt-lg-0">
        <p class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s text-center" style="font-family: 'Roboto', sans-serif;font-size:40px;font-weight:400;color:#000;font-family: 'Poppins', sans-serif;font-weight:700">Infinity   <span style="color:#000">Case </span><span style="color:#000">Study </span></p>
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col-xl-8 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <div class="card" style="background-color:#82BE00;border-radius:20px;">
          <div class="card-body">
              <div class="row">
                <div class="col-xl-6">
                    <p class="text-center" style="color:#fff; font-family: 'Roboto', sans-serif;font-weight:300;">KPI's</p>
                    <p class="text-center" style="color:#fff; font-family: 'Roboto', sans-serif;font-weight:300;">Annual Revenue <br> Generated</p>
                    <p class="text-center" style="color:#fff; font-family: 'Roboto', sans-serif;font-weight:300;">CPH</p>
                    <p class="text-center" style="color:#fff; font-family: 'Roboto', sans-serif;font-weight:300;">SPH</p>
                    <p class="text-center" style="color:#fff; font-family: 'Roboto', sans-serif;font-weight:300;">AOV</p>
                    <p class="text-center" style="color:#fff; font-family: 'Roboto', sans-serif;font-weight:300;">Cost of Acquisition (COA)</p>
                    <p class="text-center" style="color:#fff; font-family: 'Roboto', sans-serif;font-weight:300;">Average Monthly </br> Production Hours</p>
                </div>
                <div class="col-xl-6 px-5">
                  <p class="text-center" style="color:#fff;">Infinity Results</p>
                  <div class="card" style="background-color: #0078C8;border-radius:20px;">
                    <div class="card-body">
                        <p class="text-center" style="color:#fff; font-family: 'Roboto', sans-serif;font-weight:300;">$11.52M</p>
                        <p class="text-center" style="color:#fff; font-family: 'Roboto', sans-serif;font-weight:300;">7</p>
                        <p class="text-center" style="color:#fff; font-family: 'Roboto', sans-serif;font-weight:300;">0.60</p>
                        <p class="text-center" style="color:#fff; font-family: 'Roboto', sans-serif;font-weight:300;">$160</p>
                        <p class="text-center" style="color:#fff; font-family: 'Roboto', sans-serif;font-weight:300;">$96</p>
                        <p class="text-center" style="color:#fff; font-family: 'Roboto', sans-serif;font-weight:300;">10,000</p>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  </div>
</div>



<div class="container " style="margin-bottom:80px;">
  <div class="row justify-content-center">
    <div class="col-lg-10 wow animate__animated animate__fadeInUp animate__delay-0.5s">
        <p style=" font-family: 'Roboto', sans-serif;font-weight:300;">An established US provider of speech and imaging solutions was using traditional telemarketing provider for its outbound sales.</p>
        <p style=" font-family: 'Roboto', sans-serif;font-weight:300;">The client became increasingly frustrated with the provider’s lack of coordinated strategy, shared AEs, volatility in revenue production, and higher-than-expected cost of sales. </p>
        <p style=" font-family: 'Roboto', sans-serif;font-weight:300;">Infinity quickly resolved the challenges by applying the <strong> Buyerlytics® Revenue System </strong> methodology and the client saw immediate improvement to their KPIs, as illustrated, in the above graphic.</p>
      </div>
  </div>
</div>




<div class="container"  style="margin-bottom:80px;">
  <p class="text-center wow animate__animated animate__fadeInUp animate__delay-0.5s" style="font-size:40px;font-weight:400;color:#0078C8;font-family: 'Poppins', sans-serif;font-weight:700">Related <span style="color:#82BE00">Services</span></p>
  <p class="text-center mb-5 wow animate__animated animate__fadeInUp animate__delay-0.8s" style="font-size: 16px;  font-family: 'Roboto', sans-serif;
  font-weight: 300;">In the age of the digital consumer, we help you pique interest, elevate experience, nurture leads, make sales, reduce churn and grow customer <br>brand affinity with our CX driven engagement services and solutions – that support throughout the entire customer lifecycle.</p>        
  <div class="row justify-content-center">
    <div class="col-xl-3 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
      <div class="custom_overlay">
        <img src="{{asset('img/inbound 1.png')}}" class="img-fluid" style="max-width: 100%;border-radius:10px;">    
            <a href="{{route('inbound_sales')}}">
                  <div class="overlay">                
                    <div class="text">
                      <p id="solutions-title">Inbound Sales</p>
                      <a href="{{route('inbound_sales')}}" style="background-color:#82BE00;color:#fff;border-radius: 50px;" class="py-2 px-3">Learn More</a>
                    </div> 
                </div>
            </a>
       </div>
    </div>
     <div class="col-xl-3 mb-3 wow animate__animated animate__zoomIn animate__delay-500ms">
       <div class="custom_overlay">
        <img src="{{asset('img/customer 2.png')}}" class="img-fluid" style="max-width: 100%; border-radius:10px;">   
          <a href="{{route('customer_service')}}">
            <div class="overlay">                
              <div class="text">
                <p id="solutions-title">Customer Service</p>
                <a href="{{route('customer_service')}}" style="background-color:#82BE00;color:#fff;border-radius: 50px;" class="py-2 px-3">Learn More</a>
              </div> 
              
            </div>
          </a>
       </div>   
      </div>
    </div>
  </div>




@endsection

