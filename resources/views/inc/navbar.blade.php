     <!-- ======= Header ======= -->
     <header id="header" class="fixed-top d-flex align-items-center header-transparent">
      <div class="container d-flex align-items-center justify-content-between">
        <div class="logo">
          <a href="{{route('home')}}"><img src="{{asset('/img/gif-infinity.webp')}}" alt="" class="img-fluid"></a>
        </div>
        <nav id="navbar" class="navbar">
          <ul>
            <li><a class="nav-link scrollto {{'/' == request()-> path() ? 'active' : ""}}" href="{{route('home')}}">Home</a></li>
              <li class="dropdown "><a class="{{'solutions' == request()-> path() ? 'active' : ""}}" href="{{route('solutions')}}"><span>Solutions</span> <i class="bi bi-chevron-down"></i></a>
              <ul>
                <li><a href="{{route('inbound_sales')}}">Inbound Sales</a></li>              
                <li class=" {{'outbound-sales' == request()-> path() ? 'active' : ""}}"><a href="{{route('outbound_sales')}}">Outbound Sales</a></li>
                <li class=" {{'customer-service' == request()-> path() ? 'active' : ""}}"><a href="{{route('customer_service')}}">Customer Service</a></li>
              </ul>
            </li>
            <li><a class="nav-link scrollto {{'accolades' == request()-> path() ? 'active' : ""}}" href="{{route('accolades')}}">Accolades</a></li>
            <li><a class="nav-link scrollto {{'meet-our-team' == request()-> path() ? 'active' : ""}}" href="{{route('meet_our_team')}}">Our Team</a></li>
            <li><a class="nav-link scrollto {{'join-infinity' == request()-> path() ? 'active' : ""}}" href="{{route('join_infinity')}}">Join Our Team</a></li>
            <li><a class="nav-link scrollto {{'contact-us' == request()-> path() ? 'active' : ""}}" href="{{route('contact_us')}}">Contact Us</a></li>     
          </ul>
      <form class="d-flex">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-success green-btn" data-bs-toggle="modal" data-bs-target="#exampleModal">Book a Demo</button>
      </form>
          <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->
  
      </div>
    </header><!-- End Header -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content book-demo" style="border-radius: 20px;">
      <div class="row justify-content-end">
        <div style="width: 30px; height: 30px; border-radius: 50%; text-align: right; background: #1489EA; margin: 25px 25px 0 25px; display: flex; justify-content: center; align-items: center">
          <i class="fal fa-times fa-3x" style="cursor:pointer; text-align: right; font-size: 25px; color: white" data-bs-dismiss="modal" ></i>
        </div>
      </div>
      <div class="modal-body pt-0">
        <p class="px-3 mb-0" style="font-weight: 400;font-family: 'Roboto', sans-serif;font-size:35px;color:#1489EA">Book <span style="color:#82BE00"> a Demo</span> </p>
        <p class="pt-0 px-3"  style="font-weight: 300;font-family: 'Roboto', sans-serif;">Complete the form below and we will back in touch with you shortly.</p>
        <div class="container">
          <form action="{{route('book_demo')}}" method="POST">
            @csrf
          <div class="row" style="font-weight: 300;font-family: 'Roboto', sans-serif;"> 
            <div class="col-xl-6 contact-card">
                    <div class="mb-3">
                        <input type="text" name="fullname" class="form-control" id="exampleFormControlInput1" placeholder="Full Name">
                      </div>

                      <div class="mb-3">
                        <input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="Email Address">
                      </div>

                      <div class="mb-3">
                        <input type="text" name="contact" class="form-control" id="exampleFormControlInput1" placeholder="Contact Number">
                      </div>
                      
                      <div class="mb-3">
                        <input type="text" name="company" class="form-control" id="exampleFormControlInput1" placeholder="Company">
                      </div>
                </div>
         
              <div class="col-xl-6">
                <p class="mb-0">I’m interested in: <span style="color:red;">*</span></p>
             
                  <div class="form-check mb-0">
                    <input class="form-check-input" name="partner" type="checkbox" value="  Partnership Inquiry" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                      Partnership Inquiry
                    </label>
                  </div>
  
                  <div class="form-check mb-0">
                    <input class="form-check-input" name="service" type="checkbox" value=" RFP/Solutions & Services Inquiries" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                      RFP/Solutions & Services Inquiries
                    </label>
                  </div>
  
                  <div class="form-check mb-3">
                    <input class="form-check-input" name="others" type="checkbox" value="Others" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                      Other
                    </label>
                  </div>
  
                  <div class="mb-3">
                    <p class="mb-0">How can we assist you? <span style="color:red;">*</span></p>
                    <textarea name="compose_message" class="form-control text-area" id="exampleFormControlTextarea1" placeholder="Message" rows="4"></textarea>
                  </div>
                </div>
            </div>
            <div class="row justify-content-center my-4">
              <div class="col-xl-3">
                <button type="submit" style="background-color:#82BE00;color:#fff;border-radius: 50px;" class="py-2 px-5">Send</button>
              </div>
            </div>
         </form>
           
        </div>
      </div>
    </div>
  </div>
</div>
      


