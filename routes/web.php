<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\sendEmailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','infinity_pages.index')->name('home');
Route::view('/solutions','infinity_pages.solutions')->name('solutions');
Route::view('/inbound-sales','infinity_pages.inbound_sales')->name('inbound_sales');
Route::view('/outbound-sales','infinity_pages.outbound_sales')->name('outbound_sales');
Route::view('/customer-service','infinity_pages.customer_service')->name('customer_service');
Route::view('/accolades','infinity_pages.accolades')->name('accolades');
Route::view('/meet-our-team','infinity_pages.our_team')->name('meet_our_team');
Route::view('/case-study','infinity_pages.case_study')->name('case_study');
Route::view('/case-study-details','infinity_pages.case_study_details')->name('case_study_details');
Route::view('/join-infinity','infinity_pages.join_infinity')->name('join_infinity');
Route::view('/infinity-careers','infinity_pages.infinity_careers')->name('infinity_careers');
Route::view('/contact-us','infinity_pages.contact_us')->name('contact_us');

Route::post('/sendEmail',[sendEmailController::class,'send_email'])->name('send_email');
Route::post('/book-demo',[sendEmailController::class,'book_demo'])->name('book_demo');


