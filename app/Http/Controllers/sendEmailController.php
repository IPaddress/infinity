<?php

namespace App\Http\Controllers;
use App\Events\UserSentEmailEvent;
use App\Events\UserBookDemoEvent;

use Illuminate\Http\Request;

class sendEmailController extends Controller
{
    public function send_email(Request $request){

    try {
        event(new UserSentEmailEvent($request->all()));
         return redirect()->back();
        } catch (\Throwable $th) {
        return $th;
        }
    }

    public function book_demo(Request $request){
        try {
            event(new UserBookDemoEvent($request->all()));
             return redirect()->back();
            } catch (\Throwable $th) {
            return $th;
            }
        }
    }
