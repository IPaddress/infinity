<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\UserSentEmailEvent;
use App\Listeners\NotifyAdminListener;
use App\Events\UserBookDemoEvent;
use App\Listeners\NotifyBookDemo;


class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [   
            UserSentEmailEvent::class=> [
            NotifyAdminListener::class,
        ],

             UserBookDemoEvent::class=> [
            NotifyBookDemo::class,
        ],
    ];




    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
