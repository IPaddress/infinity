<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookDemo extends Mailable
{
    use Queueable, SerializesModels;
    public $fullname, $email, $contact, $company, $partner, $service, $others, $compose_message;
    public function __construct($data)
    {
        $this->fullname = $data['fullname'];
        $this->email = $data['email'];
        $this->contact = $data['contact'];
        $this->company = $data['company'];
        // $this->partner = $data['partner'];
        // $this->service = $data['service'];
        // $this->others = $data['others'];
        $this->compose_message = $data['compose_message'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->markdown('mails.book_demo')
        ->with(['fullname'=> $this->fullname, 'email'=>$this->email, 'contact'=>$this->contact, 'company' =>  $this->company, 'compose_message' => $this->compose_message])
        ->from($this->email, $this->fullname)
        ->replyTo($this->email, $this->fullname)
        ->subject('New Message from ');
    }
}
