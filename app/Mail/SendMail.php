<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class SendMail extends Mailable
{
    use Queueable, SerializesModels;
    public $name, $email, $contact, $company, $contact_description, $mesage;
    public function __construct($data)
    {
        $this->name = $data['name'];
        $this->email = $data['email'];
        $this->contact = $data['contact'];
        $this->company = $data['company'];
        $this->contact_description = $data['contact_description'];
        $this->message = $data['message'];
    }

    public function build()
    {
        
      return $this->markdown('mails.send_email')
      ->with(['name'=> $this->name, 'email'=>$this->email, 'contact'=>$this->contact, 'company' =>  $this->company, 'contact_description' => $this->contact_description, 'message' => $this->message])
      ->from($this->email, $this->name)
      ->replyTo($this->email, $this->name)
      ->subject('New Message from ');
    }
}
