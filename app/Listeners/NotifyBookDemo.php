<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\BookDemo;
use Illuminate\Support\Facades\Mail;

class NotifyBookDemo
{
    public function handle($event)
    {
        Mail::to('sienadeveloper29@gmail.com')->send(new BookDemo($event->email_data));
    }
}
