<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;


class NotifyAdminListener
{
    
    public function handle($event)
    {
        Mail::to('info@infinitydelivers.com')->send(new SendMail($event->email_data));
    }
}
